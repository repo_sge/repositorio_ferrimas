﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Interfaces
{
    interface IUnidadEquivalente
    {

        DataTable listar_unidad_equivalente();
    }
}
