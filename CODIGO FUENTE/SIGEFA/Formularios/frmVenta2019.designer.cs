﻿namespace SIGEFA.Formularios
{
    partial class tsNoImpr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(tsNoImpr));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel = new DevComponents.DotNetBar.PanelEx();
            this.line2 = new DevComponents.DotNetBar.Controls.Line();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripGuardar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripIniciaov = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripEditaov = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonPendiente = new System.Windows.Forms.ToolStripButton();
            this.toolStripAnulaov = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonSalir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripImprimir = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelx = new System.Windows.Forms.Label();
            this.txtFiltro = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnRefrescar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvproductos = new System.Windows.Forms.DataGridView();
            this.codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codUniversal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomAlma = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidad = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.nmarca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockdisponible = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codunidadmedida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codsunatimpuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codtimpuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codalma = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidadnombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgvdetalle = new System.Windows.Forms.DataGridView();
            this.coddetalle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referenci = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.product = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codunidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preciounit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montodscto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.igv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valoreal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioreal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioconigv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorpromedio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipoarticulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipoimpuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codalmacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.almacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.empres = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.chkTicket = new System.Windows.Forms.RadioButton();
            this.txtNombreVendedor = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtCodigoVendedor = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.dtpFechaPago = new System.Windows.Forms.DateTimePicker();
            this.cmbFormaPago = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.chkFactura = new System.Windows.Forms.RadioButton();
            this.chkBoleta = new System.Windows.Forms.RadioButton();
            this.dtpFecha1 = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.txtDireccion = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtNombreCliente = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtCodCliente = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.cmbMoneda = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtexoneradas = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtgravadas = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label16 = new System.Windows.Forms.Label();
            this.txtinafectas = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label15 = new System.Windows.Forms.Label();
            this.txtgratuitas = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label14 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPrecioVenta = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label9 = new System.Windows.Forms.Label();
            this.txtIGV = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label8 = new System.Windows.Forms.Label();
            this.txtValorVenta = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnAnulaOV = new DevComponents.DotNetBar.ButtonX();
            this.btnEditaOV = new DevComponents.DotNetBar.ButtonX();
            this.btnInicioOV = new DevComponents.DotNetBar.ButtonX();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.chkVentaDsctoGlobal = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkVentaGratuita = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.txtBruto = new System.Windows.Forms.TextBox();
            this.txtDscto = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.pbCapchatS = new System.Windows.Forms.PictureBox();
            this.txtSunat_Capchat = new System.Windows.Forms.TextBox();
            this.txttasa = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.lbLineaCredito = new System.Windows.Forms.Label();
            this.txtLineaCredito = new System.Windows.Forms.TextBox();
            this.txtLineaCreditoUso = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtLineaCreditoDisponible = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtCodigoBarras = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.chkVentaSinStock = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.label21 = new System.Windows.Forms.Label();
            this.lbDocumento = new System.Windows.Forms.Label();
            this.txtDocRef = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPedido = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtSerie = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX2 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBoxX5 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX6 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.superValidator1 = new DevComponents.DotNetBar.Validator.SuperValidator();
            this.requiredFieldValidator1 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Seleccionar Vendedor");
            this.dgvStockAlmacenes = new System.Windows.Forms.DataGridView();
            this.idalmacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idproductoalmacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomempresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomalmacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockalmacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbAlmacenes = new System.Windows.Forms.ComboBox();
            this.telerikMetroBlueTheme1 = new Telerik.WinControls.Themes.TelerikMetroBlueTheme();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tsNoimpre = new System.Windows.Forms.ToolStripButton();
            this.panel.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvproductos)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdetalle)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMoneda)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCapchatS)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockAlmacenes)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.CanvasColor = System.Drawing.SystemColors.Control;
            this.panel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panel.Controls.Add(this.line2);
            this.panel.Controls.Add(this.toolStrip2);
            this.panel.DisabledBackColor = System.Drawing.Color.Empty;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1170, 34);
            this.panel.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panel.Style.BorderColor.Color = System.Drawing.Color.DarkCyan;
            this.panel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panel.Style.GradientAngle = 90;
            this.panel.TabIndex = 12;
            // 
            // line2
            // 
            this.line2.AutoSize = true;
            this.line2.BackColor = System.Drawing.SystemColors.Control;
            this.line2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.line2.ForeColor = System.Drawing.Color.DarkCyan;
            this.line2.Location = new System.Drawing.Point(0, 26);
            this.line2.Name = "line2";
            this.line2.Size = new System.Drawing.Size(1170, 8);
            this.line2.TabIndex = 8;
            this.line2.Text = "line2";
            // 
            // toolStrip2
            // 
            this.toolStrip2.AllowMerge = false;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.toolStripGuardar,
            this.toolStripSeparator3,
            this.tsNoimpre,
            this.toolStripSeparator8,
            this.toolStripIniciaov,
            this.toolStripSeparator4,
            this.toolStripEditaov,
            this.toolStripSeparator5,
            this.toolStripButtonPendiente,
            this.toolStripSeparator2,
            this.toolStripAnulaov,
            this.toolStripSeparator6,
            this.toolStripButtonSalir,
            this.toolStripSeparator7,
            this.toolStripImprimir});
            this.toolStrip2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Padding = new System.Windows.Forms.Padding(0, 0, 1, 2);
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip2.Size = new System.Drawing.Size(1170, 26);
            this.toolStrip2.Stretch = true;
            this.toolStrip2.TabIndex = 7;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripGuardar
            // 
            this.toolStripGuardar.Enabled = false;
            this.toolStripGuardar.Image = ((System.Drawing.Image)(resources.GetObject("toolStripGuardar.Image")));
            this.toolStripGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripGuardar.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.toolStripGuardar.Name = "toolStripGuardar";
            this.toolStripGuardar.Size = new System.Drawing.Size(120, 20);
            this.toolStripGuardar.Text = "Guardar  (Alt + G)";
            this.toolStripGuardar.ToolTipText = "Alt + G";
            this.toolStripGuardar.Click += new System.EventHandler(this.toolStripGuardar_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripIniciaov
            // 
            this.toolStripIniciaov.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripIniciaov.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.toolStripIniciaov.Name = "toolStripIniciaov";
            this.toolStripIniciaov.Size = new System.Drawing.Size(84, 20);
            this.toolStripIniciaov.Text = "Inicia OV  (F6)";
            this.toolStripIniciaov.ToolTipText = "Inicia OV";
            this.toolStripIniciaov.Click += new System.EventHandler(this.btnInicioOV_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripEditaov
            // 
            this.toolStripEditaov.Enabled = false;
            this.toolStripEditaov.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripEditaov.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.toolStripEditaov.Name = "toolStripEditaov";
            this.toolStripEditaov.Size = new System.Drawing.Size(79, 20);
            this.toolStripEditaov.Text = "Edita OV (F4)";
            this.toolStripEditaov.ToolTipText = "F4";
            this.toolStripEditaov.Click += new System.EventHandler(this.btnEditaOV_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripButtonPendiente
            // 
            this.toolStripButtonPendiente.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPendiente.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.toolStripButtonPendiente.Name = "toolStripButtonPendiente";
            this.toolStripButtonPendiente.Size = new System.Drawing.Size(69, 20);
            this.toolStripButtonPendiente.Text = "Pendientes";
            this.toolStripButtonPendiente.ToolTipText = "Anula OV";
            this.toolStripButtonPendiente.Click += new System.EventHandler(this.toolStripButtonPendiente_Click);
            // 
            // toolStripAnulaov
            // 
            this.toolStripAnulaov.Enabled = false;
            this.toolStripAnulaov.Image = global::SIGEFA.Properties.Resources.x_button;
            this.toolStripAnulaov.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAnulaov.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.toolStripAnulaov.Name = "toolStripAnulaov";
            this.toolStripAnulaov.Size = new System.Drawing.Size(77, 20);
            this.toolStripAnulaov.Text = "Anula OV";
            this.toolStripAnulaov.ToolTipText = "Anula OV";
            this.toolStripAnulaov.Visible = false;
            this.toolStripAnulaov.Click += new System.EventHandler(this.toolStripAnulaov_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 24);
            this.toolStripSeparator6.Visible = false;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripButtonSalir
            // 
            this.toolStripButtonSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSalir.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.toolStripButtonSalir.Name = "toolStripButtonSalir";
            this.toolStripButtonSalir.Size = new System.Drawing.Size(33, 20);
            this.toolStripButtonSalir.Text = "Salir";
            this.toolStripButtonSalir.ToolTipText = "Esc";
            this.toolStripButtonSalir.Click += new System.EventHandler(this.toolStripButtonSalir_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 24);
            // 
            // toolStripImprimir
            // 
            this.toolStripImprimir.Enabled = false;
            this.toolStripImprimir.Image = ((System.Drawing.Image)(resources.GetObject("toolStripImprimir.Image")));
            this.toolStripImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripImprimir.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.toolStripImprimir.Name = "toolStripImprimir";
            this.toolStripImprimir.Size = new System.Drawing.Size(73, 20);
            this.toolStripImprimir.Text = "Imprimir";
            this.toolStripImprimir.Click += new System.EventHandler(this.toolStripImprimir_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.labelx);
            this.groupBox1.Controls.Add(this.txtFiltro);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.btnRefrescar);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(94, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(529, 51);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "BUSCAR";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // labelx
            // 
            this.labelx.AutoSize = true;
            this.labelx.Location = new System.Drawing.Point(441, 23);
            this.labelx.Name = "labelx";
            this.labelx.Size = new System.Drawing.Size(13, 13);
            this.labelx.TabIndex = 309;
            this.labelx.Text = "x";
            this.labelx.Visible = false;
            // 
            // txtFiltro
            // 
            this.txtFiltro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFiltro.Location = new System.Drawing.Point(220, 19);
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.Size = new System.Drawing.Size(218, 22);
            this.txtFiltro.TabIndex = 306;
            this.toolTip1.SetToolTip(this.txtFiltro, "Alt + B");
            this.txtFiltro.TextChanged += new System.EventHandler(this.txtFiltro_TextChanged);
            this.txtFiltro.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFiltro_KeyDown);
            this.txtFiltro.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFiltro_KeyUp);
            this.txtFiltro.Leave += new System.EventHandler(this.txtFiltro_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(2, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 13);
            this.label10.TabIndex = 307;
            this.label10.Text = "Buscar Por(Alt+B) :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(107, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 16);
            this.label11.TabIndex = 308;
            this.label11.Text = "X";
            // 
            // btnRefrescar
            // 
            this.btnRefrescar.BackColor = System.Drawing.Color.Transparent;
            this.btnRefrescar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnRefrescar.FlatAppearance.BorderSize = 0;
            this.btnRefrescar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Khaki;
            this.btnRefrescar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnRefrescar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefrescar.Image = ((System.Drawing.Image)(resources.GetObject("btnRefrescar.Image")));
            this.btnRefrescar.Location = new System.Drawing.Point(473, 9);
            this.btnRefrescar.Margin = new System.Windows.Forms.Padding(0);
            this.btnRefrescar.Name = "btnRefrescar";
            this.btnRefrescar.Size = new System.Drawing.Size(48, 33);
            this.btnRefrescar.TabIndex = 305;
            this.toolTip1.SetToolTip(this.btnRefrescar, "Ctrl + R");
            this.btnRefrescar.UseVisualStyleBackColor = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dgvproductos);
            this.groupBox2.Enabled = false;
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(12, 94);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(878, 249);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PRODUCTOS (Alt+P)";
            // 
            // dgvproductos
            // 
            this.dgvproductos.AllowUserToAddRows = false;
            this.dgvproductos.AllowUserToDeleteRows = false;
            this.dgvproductos.AllowUserToResizeRows = false;
            this.dgvproductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvproductos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(124)))), ((int)(((byte)(210)))));
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.Menu;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvproductos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this.dgvproductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvproductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codigo,
            this.referencia,
            this.codUniversal,
            this.nomAlma,
            this.descripcion,
            this.unidad,
            this.nmarca,
            this.Modelo,
            this.stockdisponible,
            this.cant,
            this.precio,
            this.total,
            this.codunidadmedida,
            this.codsunatimpuesto,
            this.codtimpuesto,
            this.codalma,
            this.unidadnombre});
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvproductos.DefaultCellStyle = dataGridViewCellStyle36;
            this.dgvproductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvproductos.EnableHeadersVisualStyles = false;
            this.dgvproductos.Location = new System.Drawing.Point(3, 18);
            this.dgvproductos.MultiSelect = false;
            this.dgvproductos.Name = "dgvproductos";
            this.dgvproductos.RowHeadersVisible = false;
            this.dgvproductos.RowTemplate.Height = 28;
            this.dgvproductos.Size = new System.Drawing.Size(872, 228);
            this.dgvproductos.TabIndex = 1;
            this.dgvproductos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvproductos_CellClick);
            this.dgvproductos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvproductos_CellContentClick);
            this.dgvproductos.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvproductos_CellEndEdit);
            this.dgvproductos.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvproductos_ColumnHeaderMouseClick);
            this.dgvproductos.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvproductos_EditingControlShowing);
            // 
            // codigo
            // 
            this.codigo.DataPropertyName = "codProducto";
            this.codigo.HeaderText = "Codigo ";
            this.codigo.Name = "codigo";
            this.codigo.ReadOnly = true;
            this.codigo.Visible = false;
            // 
            // referencia
            // 
            this.referencia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.referencia.DataPropertyName = "referencia";
            this.referencia.HeaderText = "Referencia";
            this.referencia.Name = "referencia";
            this.referencia.ReadOnly = true;
            this.referencia.Width = 86;
            // 
            // codUniversal
            // 
            this.codUniversal.DataPropertyName = "codUniversal";
            this.codUniversal.HeaderText = "Cod Universal";
            this.codUniversal.Name = "codUniversal";
            this.codUniversal.ReadOnly = true;
            this.codUniversal.Visible = false;
            // 
            // nomAlma
            // 
            this.nomAlma.DataPropertyName = "nomAlma";
            this.nomAlma.HeaderText = "Almacen";
            this.nomAlma.Name = "nomAlma";
            // 
            // descripcion
            // 
            this.descripcion.DataPropertyName = "descripcion";
            this.descripcion.HeaderText = "Descripcion";
            this.descripcion.Name = "descripcion";
            this.descripcion.ReadOnly = true;
            // 
            // unidad
            // 
            this.unidad.DisplayStyleForCurrentCellOnly = true;
            this.unidad.HeaderText = "Unidad";
            this.unidad.Name = "unidad";
            this.unidad.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.unidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // nmarca
            // 
            this.nmarca.DataPropertyName = "nmarca";
            this.nmarca.HeaderText = "Marca";
            this.nmarca.Name = "nmarca";
            this.nmarca.ReadOnly = true;
            this.nmarca.Visible = false;
            // 
            // Modelo
            // 
            this.Modelo.DataPropertyName = "modelo";
            this.Modelo.HeaderText = "Modelo";
            this.Modelo.Name = "Modelo";
            this.Modelo.ReadOnly = true;
            this.Modelo.Visible = false;
            // 
            // stockdisponible
            // 
            this.stockdisponible.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.stockdisponible.DataPropertyName = "stockdisponible";
            this.stockdisponible.HeaderText = "Stock";
            this.stockdisponible.Name = "stockdisponible";
            this.stockdisponible.ReadOnly = true;
            this.stockdisponible.Width = 60;
            // 
            // cant
            // 
            this.cant.HeaderText = "Cantidad";
            this.cant.Name = "cant";
            // 
            // precio
            // 
            this.precio.HeaderText = "Precio";
            this.precio.Name = "precio";
            // 
            // total
            // 
            this.total.HeaderText = "Total";
            this.total.Name = "total";
            this.total.ReadOnly = true;
            // 
            // codunidadmedida
            // 
            this.codunidadmedida.DataPropertyName = "codunidadmedida";
            this.codunidadmedida.HeaderText = "CodUnidad";
            this.codunidadmedida.Name = "codunidadmedida";
            this.codunidadmedida.ReadOnly = true;
            this.codunidadmedida.Visible = false;
            // 
            // codsunatimpuesto
            // 
            this.codsunatimpuesto.DataPropertyName = "codsunatimpuesto";
            this.codsunatimpuesto.HeaderText = "Codigo Sunat";
            this.codsunatimpuesto.Name = "codsunatimpuesto";
            this.codsunatimpuesto.ReadOnly = true;
            this.codsunatimpuesto.Visible = false;
            // 
            // codtimpuesto
            // 
            this.codtimpuesto.DataPropertyName = "codtimpuesto";
            this.codtimpuesto.HeaderText = "CodTipoImpuesto";
            this.codtimpuesto.Name = "codtimpuesto";
            this.codtimpuesto.ReadOnly = true;
            this.codtimpuesto.Visible = false;
            // 
            // codalma
            // 
            this.codalma.DataPropertyName = "codalma";
            this.codalma.HeaderText = "codalma";
            this.codalma.Name = "codalma";
            this.codalma.Visible = false;
            // 
            // unidadnombre
            // 
            this.unidadnombre.DataPropertyName = "unidadnombre";
            this.unidadnombre.HeaderText = "UnidadNombre";
            this.unidadnombre.Name = "unidadnombre";
            this.unidadnombre.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.dgvdetalle);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox3.Location = new System.Drawing.Point(11, 349);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(878, 263);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "DETALLE ORDEN";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // dgvdetalle
            // 
            this.dgvdetalle.AllowUserToAddRows = false;
            this.dgvdetalle.AllowUserToDeleteRows = false;
            this.dgvdetalle.AllowUserToResizeRows = false;
            this.dgvdetalle.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvdetalle.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(124)))), ((int)(((byte)(210)))));
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.Menu;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvdetalle.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
            this.dgvdetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvdetalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.coddetalle,
            this.codproducto,
            this.referenci,
            this.product,
            this.codunidad,
            this.unida,
            this.cantidad,
            this.preciounit,
            this.importe,
            this.dscto1,
            this.dscto2,
            this.dscto3,
            this.montodscto,
            this.valorventa,
            this.igv,
            this.precioventa,
            this.valoreal,
            this.precioreal,
            this.precioconigv,
            this.valorpromedio,
            this.Tipoarticulo,
            this.Tipoimpuesto,
            this.codalmacen,
            this.almacen,
            this.TipoUnidad,
            this.empres});
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle50.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle50.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle50.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle50.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle50.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvdetalle.DefaultCellStyle = dataGridViewCellStyle50;
            this.dgvdetalle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvdetalle.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvdetalle.EnableHeadersVisualStyles = false;
            this.dgvdetalle.Location = new System.Drawing.Point(3, 18);
            this.dgvdetalle.Name = "dgvdetalle";
            this.dgvdetalle.RowHeadersVisible = false;
            this.dgvdetalle.RowTemplate.Height = 28;
            this.dgvdetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvdetalle.Size = new System.Drawing.Size(872, 242);
            this.dgvdetalle.TabIndex = 3;
            this.dgvdetalle.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.dgvdetalle_PreviewKeyDown);
            // 
            // coddetalle
            // 
            this.coddetalle.DataPropertyName = "codDetalle";
            this.coddetalle.HeaderText = "CodDetalle";
            this.coddetalle.Name = "coddetalle";
            this.coddetalle.ReadOnly = true;
            this.coddetalle.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.coddetalle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.coddetalle.Visible = false;
            // 
            // codproducto
            // 
            this.codproducto.DataPropertyName = "codProducto";
            this.codproducto.HeaderText = "Código de Producto_";
            this.codproducto.Name = "codproducto";
            this.codproducto.ReadOnly = true;
            this.codproducto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.codproducto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.codproducto.Visible = false;
            // 
            // referenci
            // 
            this.referenci.DataPropertyName = "referencia";
            this.referenci.HeaderText = "Código de Producto";
            this.referenci.Name = "referenci";
            this.referenci.ReadOnly = true;
            this.referenci.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // product
            // 
            this.product.DataPropertyName = "producto";
            this.product.HeaderText = "Descripción";
            this.product.Name = "product";
            this.product.ReadOnly = true;
            this.product.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // codunidad
            // 
            this.codunidad.DataPropertyName = "codUnidadMedida";
            this.codunidad.HeaderText = "Cod. Unidad";
            this.codunidad.Name = "codunidad";
            this.codunidad.ReadOnly = true;
            this.codunidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.codunidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.codunidad.Visible = false;
            // 
            // unida
            // 
            this.unida.DataPropertyName = "unidad";
            this.unida.HeaderText = "Unidad";
            this.unida.Name = "unida";
            this.unida.ReadOnly = true;
            this.unida.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.unida.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cantidad
            // 
            this.cantidad.DataPropertyName = "cantidad";
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle38.Format = "N2";
            dataGridViewCellStyle38.NullValue = null;
            this.cantidad.DefaultCellStyle = dataGridViewCellStyle38;
            this.cantidad.HeaderText = "Cantidad";
            this.cantidad.Name = "cantidad";
            this.cantidad.ReadOnly = true;
            this.cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // preciounit
            // 
            this.preciounit.DataPropertyName = "preciounitario";
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle39.Format = "N2";
            this.preciounit.DefaultCellStyle = dataGridViewCellStyle39;
            this.preciounit.HeaderText = "P. Unit.";
            this.preciounit.Name = "preciounit";
            this.preciounit.ReadOnly = true;
            this.preciounit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.preciounit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // importe
            // 
            this.importe.DataPropertyName = "subtotal";
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle40.Format = "N2";
            dataGridViewCellStyle40.NullValue = null;
            this.importe.DefaultCellStyle = dataGridViewCellStyle40;
            this.importe.HeaderText = "Importe";
            this.importe.Name = "importe";
            this.importe.ReadOnly = true;
            this.importe.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.importe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.importe.Visible = false;
            // 
            // dscto1
            // 
            this.dscto1.DataPropertyName = "descuento1";
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle41.Format = "N2";
            this.dscto1.DefaultCellStyle = dataGridViewCellStyle41;
            this.dscto1.HeaderText = "% Dscto1";
            this.dscto1.Name = "dscto1";
            this.dscto1.ReadOnly = true;
            this.dscto1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto1.Visible = false;
            // 
            // dscto2
            // 
            this.dscto2.DataPropertyName = "descuento2";
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle42.Format = "N2";
            this.dscto2.DefaultCellStyle = dataGridViewCellStyle42;
            this.dscto2.HeaderText = "% Dscto2";
            this.dscto2.Name = "dscto2";
            this.dscto2.ReadOnly = true;
            this.dscto2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto2.Visible = false;
            // 
            // dscto3
            // 
            this.dscto3.DataPropertyName = "descuento3";
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle43.Format = "N2";
            this.dscto3.DefaultCellStyle = dataGridViewCellStyle43;
            this.dscto3.HeaderText = "% Dscto3";
            this.dscto3.Name = "dscto3";
            this.dscto3.ReadOnly = true;
            this.dscto3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto3.Visible = false;
            // 
            // montodscto
            // 
            this.montodscto.DataPropertyName = "montodscto";
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle44.Format = "N2";
            dataGridViewCellStyle44.NullValue = null;
            this.montodscto.DefaultCellStyle = dataGridViewCellStyle44;
            this.montodscto.HeaderText = "Monto Dscto";
            this.montodscto.Name = "montodscto";
            this.montodscto.ReadOnly = true;
            this.montodscto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.montodscto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.montodscto.Visible = false;
            // 
            // valorventa
            // 
            this.valorventa.DataPropertyName = "valorventa";
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle45.Format = "N2";
            dataGridViewCellStyle45.NullValue = null;
            this.valorventa.DefaultCellStyle = dataGridViewCellStyle45;
            this.valorventa.HeaderText = "V. Venta";
            this.valorventa.Name = "valorventa";
            this.valorventa.ReadOnly = true;
            this.valorventa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valorventa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.valorventa.Visible = false;
            // 
            // igv
            // 
            this.igv.DataPropertyName = "igv";
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle46.Format = "N2";
            this.igv.DefaultCellStyle = dataGridViewCellStyle46;
            this.igv.HeaderText = "IGV";
            this.igv.Name = "igv";
            this.igv.ReadOnly = true;
            this.igv.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.igv.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.igv.Visible = false;
            // 
            // precioventa
            // 
            this.precioventa.DataPropertyName = "importe";
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle47.Format = "N2";
            this.precioventa.DefaultCellStyle = dataGridViewCellStyle47;
            this.precioventa.HeaderText = "P. Venta";
            this.precioventa.Name = "precioventa";
            this.precioventa.ReadOnly = true;
            this.precioventa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.precioventa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // valoreal
            // 
            this.valoreal.DataPropertyName = "valoreal";
            dataGridViewCellStyle48.Format = "N2";
            dataGridViewCellStyle48.NullValue = null;
            this.valoreal.DefaultCellStyle = dataGridViewCellStyle48;
            this.valoreal.HeaderText = "V. real";
            this.valoreal.Name = "valoreal";
            this.valoreal.ReadOnly = true;
            this.valoreal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valoreal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.valoreal.Visible = false;
            // 
            // precioreal
            // 
            this.precioreal.DataPropertyName = "precioreal";
            dataGridViewCellStyle49.Format = "N2";
            dataGridViewCellStyle49.NullValue = null;
            this.precioreal.DefaultCellStyle = dataGridViewCellStyle49;
            this.precioreal.HeaderText = "P. real";
            this.precioreal.Name = "precioreal";
            this.precioreal.ReadOnly = true;
            this.precioreal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.precioreal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.precioreal.Visible = false;
            // 
            // precioconigv
            // 
            this.precioconigv.DataPropertyName = "precioigv";
            this.precioconigv.HeaderText = "precioconigv";
            this.precioconigv.Name = "precioconigv";
            this.precioconigv.ReadOnly = true;
            this.precioconigv.Visible = false;
            // 
            // valorpromedio
            // 
            this.valorpromedio.DataPropertyName = "valorpromedio";
            this.valorpromedio.HeaderText = "valorpromedio";
            this.valorpromedio.Name = "valorpromedio";
            this.valorpromedio.ReadOnly = true;
            this.valorpromedio.Visible = false;
            // 
            // Tipoarticulo
            // 
            this.Tipoarticulo.DataPropertyName = "Tipoarticulo";
            this.Tipoarticulo.HeaderText = "Tipoarticulo";
            this.Tipoarticulo.Name = "Tipoarticulo";
            this.Tipoarticulo.ReadOnly = true;
            this.Tipoarticulo.Visible = false;
            // 
            // Tipoimpuesto
            // 
            this.Tipoimpuesto.DataPropertyName = "Tipoimpuesto";
            this.Tipoimpuesto.HeaderText = "Tipoimpuesto";
            this.Tipoimpuesto.Name = "Tipoimpuesto";
            this.Tipoimpuesto.ReadOnly = true;
            this.Tipoimpuesto.Visible = false;
            // 
            // codalmacen
            // 
            this.codalmacen.DataPropertyName = "codalmacen";
            this.codalmacen.HeaderText = "codalmacen";
            this.codalmacen.Name = "codalmacen";
            this.codalmacen.ReadOnly = true;
            this.codalmacen.Visible = false;
            // 
            // almacen
            // 
            this.almacen.DataPropertyName = "almacen";
            this.almacen.HeaderText = "Almacen";
            this.almacen.Name = "almacen";
            this.almacen.ReadOnly = true;
            // 
            // TipoUnidad
            // 
            this.TipoUnidad.HeaderText = "TipoUnidad";
            this.TipoUnidad.Name = "TipoUnidad";
            this.TipoUnidad.ReadOnly = true;
            this.TipoUnidad.Visible = false;
            // 
            // empres
            // 
            this.empres.DataPropertyName = "empres";
            this.empres.HeaderText = "Empresa";
            this.empres.Name = "empres";
            this.empres.ReadOnly = true;
            this.empres.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.dtpFecha);
            this.groupBox4.Controls.Add(this.labelX6);
            this.groupBox4.Controls.Add(this.labelX5);
            this.groupBox4.Controls.Add(this.labelX4);
            this.groupBox4.Controls.Add(this.labelX3);
            this.groupBox4.Controls.Add(this.labelX2);
            this.groupBox4.Controls.Add(this.labelX1);
            this.groupBox4.Controls.Add(this.chkTicket);
            this.groupBox4.Controls.Add(this.txtNombreVendedor);
            this.groupBox4.Controls.Add(this.txtCodigoVendedor);
            this.groupBox4.Controls.Add(this.dtpFechaPago);
            this.groupBox4.Controls.Add(this.cmbFormaPago);
            this.groupBox4.Controls.Add(this.chkFactura);
            this.groupBox4.Controls.Add(this.chkBoleta);
            this.groupBox4.Controls.Add(this.dtpFecha1);
            this.groupBox4.Controls.Add(this.txtDireccion);
            this.groupBox4.Controls.Add(this.txtNombreCliente);
            this.groupBox4.Controls.Add(this.txtCodCliente);
            this.groupBox4.Controls.Add(this.labelX7);
            this.groupBox4.Controls.Add(this.cmbMoneda);
            this.groupBox4.Enabled = false;
            this.groupBox4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox4.Location = new System.Drawing.Point(905, 102);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(371, 221);
            this.groupBox4.TabIndex = 23;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "DATOS DEL CLIENTE";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // dtpFecha
            // 
            this.dtpFecha.Enabled = false;
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(73, 143);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(181, 22);
            this.dtpFecha.TabIndex = 131;
            this.dtpFecha.Tag = "16";
            this.dtpFecha.Value = new System.DateTime(2019, 5, 23, 0, 0, 0, 0);
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX6.Location = new System.Drawing.Point(21, 175);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(49, 13);
            this.labelX6.TabIndex = 130;
            this.labelX6.Text = "F. Pago:";
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX5.Location = new System.Drawing.Point(29, 151);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(38, 10);
            this.labelX5.TabIndex = 129;
            this.labelX5.Text = "Fecha:";
            this.labelX5.Click += new System.EventHandler(this.labelX5_Click);
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX4.Location = new System.Drawing.Point(11, 112);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(60, 33);
            this.labelX4.TabIndex = 128;
            this.labelX4.Text = "Vendedor: (Alt+V)";
            this.labelX4.WordWrap = true;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX3.Location = new System.Drawing.Point(12, 76);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(55, 28);
            this.labelX3.TabIndex = 127;
            this.labelX3.Text = "Direccion: (Alt+D)";
            this.labelX3.WordWrap = true;
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX2.Location = new System.Drawing.Point(23, 53);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(48, 11);
            this.labelX2.TabIndex = 126;
            this.labelX2.Text = "Cliente:";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX1.Location = new System.Drawing.Point(20, 15);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(47, 28);
            this.labelX1.TabIndex = 125;
            this.labelX1.Text = "Ruc/Dni (Alt+R):";
            this.labelX1.WordWrap = true;
            this.labelX1.Click += new System.EventHandler(this.labelX1_Click);
            // 
            // chkTicket
            // 
            this.chkTicket.AutoCheck = false;
            this.chkTicket.Enabled = false;
            this.chkTicket.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkTicket.Location = new System.Drawing.Point(269, 198);
            this.chkTicket.Name = "chkTicket";
            this.chkTicket.Size = new System.Drawing.Size(60, 17);
            this.chkTicket.TabIndex = 124;
            this.chkTicket.TabStop = true;
            this.chkTicket.Text = "TICKET";
            this.chkTicket.UseVisualStyleBackColor = true;
            this.chkTicket.Click += new System.EventHandler(this.chkTicket_Click);
            // 
            // txtNombreVendedor
            // 
            // 
            // 
            // 
            this.txtNombreVendedor.Border.Class = "TextBoxBorder";
            this.txtNombreVendedor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtNombreVendedor.Location = new System.Drawing.Point(134, 115);
            this.txtNombreVendedor.Name = "txtNombreVendedor";
            this.txtNombreVendedor.PreventEnterBeep = true;
            this.txtNombreVendedor.Size = new System.Drawing.Size(191, 22);
            this.txtNombreVendedor.TabIndex = 123;
            this.txtNombreVendedor.Text = "<--  SELECCIONE UN VENDEDOR";
            // 
            // txtCodigoVendedor
            // 
            // 
            // 
            // 
            this.txtCodigoVendedor.Border.Class = "TextBoxBorder";
            this.txtCodigoVendedor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodigoVendedor.Location = new System.Drawing.Point(73, 115);
            this.txtCodigoVendedor.Name = "txtCodigoVendedor";
            this.txtCodigoVendedor.PreventEnterBeep = true;
            this.txtCodigoVendedor.Size = new System.Drawing.Size(54, 22);
            this.txtCodigoVendedor.TabIndex = 122;
            this.toolTip1.SetToolTip(this.txtCodigoVendedor, "Alt + V");
            this.superValidator1.SetValidator1(this.txtCodigoVendedor, this.requiredFieldValidator1);
            this.txtCodigoVendedor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigoVendedor_KeyDown);
            this.txtCodigoVendedor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCodigoVendedor_KeyUp);
            // 
            // dtpFechaPago
            // 
            this.dtpFechaPago.Enabled = false;
            this.dtpFechaPago.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaPago.Location = new System.Drawing.Point(263, 170);
            this.dtpFechaPago.Name = "dtpFechaPago";
            this.dtpFechaPago.Size = new System.Drawing.Size(93, 22);
            this.dtpFechaPago.TabIndex = 120;
            this.dtpFechaPago.Tag = "16";
            this.dtpFechaPago.Value = new System.DateTime(2019, 5, 23, 0, 0, 0, 0);
            this.dtpFechaPago.Visible = false;
            // 
            // cmbFormaPago
            // 
            this.cmbFormaPago.DisplayMember = "Text";
            this.cmbFormaPago.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbFormaPago.FormattingEnabled = true;
            this.cmbFormaPago.ItemHeight = 16;
            this.cmbFormaPago.Location = new System.Drawing.Point(73, 170);
            this.cmbFormaPago.Name = "cmbFormaPago";
            this.cmbFormaPago.Size = new System.Drawing.Size(181, 22);
            this.cmbFormaPago.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbFormaPago.TabIndex = 11;
            this.cmbFormaPago.SelectionChangeCommitted += new System.EventHandler(this.cmbFormaPago_SelectionChangeCommitted);
            // 
            // chkFactura
            // 
            this.chkFactura.Enabled = false;
            this.chkFactura.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.chkFactura.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkFactura.Location = new System.Drawing.Point(170, 198);
            this.chkFactura.Name = "chkFactura";
            this.chkFactura.Size = new System.Drawing.Size(80, 18);
            this.chkFactura.TabIndex = 9;
            this.chkFactura.Text = "FACTURA";
            this.chkFactura.UseVisualStyleBackColor = true;
            this.chkFactura.Click += new System.EventHandler(this.chkFactura_Click);
            // 
            // chkBoleta
            // 
            this.chkBoleta.AutoSize = true;
            this.chkBoleta.Checked = true;
            this.chkBoleta.Enabled = false;
            this.chkBoleta.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.chkBoleta.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkBoleta.Location = new System.Drawing.Point(84, 198);
            this.chkBoleta.Name = "chkBoleta";
            this.chkBoleta.Size = new System.Drawing.Size(71, 18);
            this.chkBoleta.TabIndex = 8;
            this.chkBoleta.TabStop = true;
            this.chkBoleta.Text = "BOLETA";
            this.chkBoleta.UseVisualStyleBackColor = true;
            this.chkBoleta.Click += new System.EventHandler(this.chkBoleta_Click);
            // 
            // dtpFecha1
            // 
            // 
            // 
            // 
            this.dtpFecha1.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpFecha1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFecha1.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpFecha1.ButtonDropDown.Visible = true;
            this.dtpFecha1.DateTimeSelectorVisibility = DevComponents.Editors.DateTimeAdv.eDateTimeSelectorVisibility.Both;
            this.dtpFecha1.IsPopupCalendarOpen = false;
            this.dtpFecha1.Location = new System.Drawing.Point(284, 142);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dtpFecha1.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFecha1.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dtpFecha1.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpFecha1.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpFecha1.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpFecha1.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpFecha1.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpFecha1.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpFecha1.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpFecha1.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFecha1.MonthCalendar.DayClickAutoClosePopup = false;
            this.dtpFecha1.MonthCalendar.DisplayMonth = new System.DateTime(2018, 12, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dtpFecha1.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpFecha1.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpFecha1.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpFecha1.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpFecha1.MonthCalendar.TodayButtonVisible = true;
            this.dtpFecha1.Name = "dtpFecha1";
            this.dtpFecha1.Size = new System.Drawing.Size(54, 22);
            this.dtpFecha1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpFecha1.TabIndex = 7;
            this.dtpFecha1.Visible = false;
            // 
            // txtDireccion
            // 
            // 
            // 
            // 
            this.txtDireccion.Border.Class = "TextBoxBorder";
            this.txtDireccion.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDireccion.Location = new System.Drawing.Point(73, 74);
            this.txtDireccion.Multiline = true;
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.PreventEnterBeep = true;
            this.txtDireccion.Size = new System.Drawing.Size(252, 35);
            this.txtDireccion.TabIndex = 5;
            this.toolTip1.SetToolTip(this.txtDireccion, "Alt + D");
            this.txtDireccion.DoubleClick += new System.EventHandler(this.txtDireccion_DoubleClick);
            // 
            // txtNombreCliente
            // 
            // 
            // 
            // 
            this.txtNombreCliente.Border.Class = "TextBoxBorder";
            this.txtNombreCliente.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtNombreCliente.Location = new System.Drawing.Point(73, 46);
            this.txtNombreCliente.Name = "txtNombreCliente";
            this.txtNombreCliente.PreventEnterBeep = true;
            this.txtNombreCliente.Size = new System.Drawing.Size(252, 22);
            this.txtNombreCliente.TabIndex = 3;
            this.toolTip1.SetToolTip(this.txtNombreCliente, "Alt + C");
            this.txtNombreCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombreCliente_KeyPress);
            // 
            // txtCodCliente
            // 
            // 
            // 
            // 
            this.txtCodCliente.Border.Class = "TextBoxBorder";
            this.txtCodCliente.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodCliente.Location = new System.Drawing.Point(73, 18);
            this.txtCodCliente.Name = "txtCodCliente";
            this.txtCodCliente.PreventEnterBeep = true;
            this.txtCodCliente.Size = new System.Drawing.Size(134, 22);
            this.txtCodCliente.TabIndex = 1;
            this.toolTip1.SetToolTip(this.txtCodCliente, "Alt + R");
            this.txtCodCliente.DoubleClick += new System.EventHandler(this.txtCodCliente_DoubleClick);
            this.txtCodCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodCliente_KeyDown);
            this.txtCodCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCliente_KeyPress);
            this.txtCodCliente.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCodCliente_KeyUp);
            // 
            // labelX7
            // 
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelX7.Location = new System.Drawing.Point(200, 13);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(47, 28);
            this.labelX7.TabIndex = 133;
            this.labelX7.Text = "Moneda:";
            this.labelX7.Visible = false;
            this.labelX7.WordWrap = true;
            this.labelX7.Click += new System.EventHandler(this.labelX7_Click);
            // 
            // cmbMoneda
            // 
            this.cmbMoneda.Enabled = false;
            this.cmbMoneda.Location = new System.Drawing.Point(255, 16);
            this.cmbMoneda.Name = "cmbMoneda";
            this.cmbMoneda.Size = new System.Drawing.Size(87, 24);
            this.cmbMoneda.TabIndex = 132;
            this.cmbMoneda.ThemeName = "TelerikMetroBlue";
            this.cmbMoneda.Visible = false;
            this.cmbMoneda.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cmbMoneda_SelectedIndexChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.txtexoneradas);
            this.groupBox5.Controls.Add(this.txtgravadas);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.txtinafectas);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.txtgratuitas);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.txtPrecioVenta);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.txtIGV);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.txtValorVenta);
            this.groupBox5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox5.Location = new System.Drawing.Point(905, 438);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(371, 174);
            this.groupBox5.TabIndex = 24;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "TOTALES";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // txtexoneradas
            // 
            this.txtexoneradas.BackColor = System.Drawing.Color.AliceBlue;
            // 
            // 
            // 
            this.txtexoneradas.Border.Class = "TextBoxBorder";
            this.txtexoneradas.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtexoneradas.Enabled = false;
            this.txtexoneradas.Location = new System.Drawing.Point(233, 27);
            this.txtexoneradas.Name = "txtexoneradas";
            this.txtexoneradas.PreventEnterBeep = true;
            this.txtexoneradas.ReadOnly = true;
            this.txtexoneradas.Size = new System.Drawing.Size(96, 22);
            this.txtexoneradas.TabIndex = 21;
            // 
            // txtgravadas
            // 
            this.txtgravadas.BackColor = System.Drawing.Color.AliceBlue;
            // 
            // 
            // 
            this.txtgravadas.Border.Class = "TextBoxBorder";
            this.txtgravadas.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtgravadas.Enabled = false;
            this.txtgravadas.Location = new System.Drawing.Point(95, 27);
            this.txtgravadas.Name = "txtgravadas";
            this.txtgravadas.PreventEnterBeep = true;
            this.txtgravadas.ReadOnly = true;
            this.txtgravadas.Size = new System.Drawing.Size(86, 22);
            this.txtgravadas.TabIndex = 20;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(198, 68);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "Inaf :";
            // 
            // txtinafectas
            // 
            this.txtinafectas.BackColor = System.Drawing.Color.AliceBlue;
            // 
            // 
            // 
            this.txtinafectas.Border.Class = "TextBoxBorder";
            this.txtinafectas.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtinafectas.Enabled = false;
            this.txtinafectas.Location = new System.Drawing.Point(233, 65);
            this.txtinafectas.Name = "txtinafectas";
            this.txtinafectas.PreventEnterBeep = true;
            this.txtinafectas.ReadOnly = true;
            this.txtinafectas.Size = new System.Drawing.Size(96, 22);
            this.txtinafectas.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(45, 68);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Grat :";
            // 
            // txtgratuitas
            // 
            this.txtgratuitas.BackColor = System.Drawing.Color.AliceBlue;
            // 
            // 
            // 
            this.txtgratuitas.Border.Class = "TextBoxBorder";
            this.txtgratuitas.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtgratuitas.Enabled = false;
            this.txtgratuitas.Location = new System.Drawing.Point(95, 65);
            this.txtgratuitas.Name = "txtgratuitas";
            this.txtgratuitas.PreventEnterBeep = true;
            this.txtgratuitas.ReadOnly = true;
            this.txtgratuitas.Size = new System.Drawing.Size(86, 22);
            this.txtgratuitas.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(192, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Exon :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Grav :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 107);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "SubTotal :";
            // 
            // txtPrecioVenta
            // 
            this.txtPrecioVenta.BackColor = System.Drawing.Color.AliceBlue;
            // 
            // 
            // 
            this.txtPrecioVenta.Border.Class = "TextBoxBorder";
            this.txtPrecioVenta.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPrecioVenta.Enabled = false;
            this.txtPrecioVenta.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.txtPrecioVenta.Location = new System.Drawing.Point(158, 137);
            this.txtPrecioVenta.Name = "txtPrecioVenta";
            this.txtPrecioVenta.PreventEnterBeep = true;
            this.txtPrecioVenta.ReadOnly = true;
            this.txtPrecioVenta.Size = new System.Drawing.Size(117, 32);
            this.txtPrecioVenta.TabIndex = 10;
            this.txtPrecioVenta.TextChanged += new System.EventHandler(this.txtPrecioVenta_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label9.Location = new System.Drawing.Point(86, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 25);
            this.label9.TabIndex = 9;
            this.label9.Text = "TOTAL";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // txtIGV
            // 
            this.txtIGV.BackColor = System.Drawing.Color.AliceBlue;
            // 
            // 
            // 
            this.txtIGV.Border.Class = "TextBoxBorder";
            this.txtIGV.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtIGV.Enabled = false;
            this.txtIGV.Location = new System.Drawing.Point(233, 100);
            this.txtIGV.Name = "txtIGV";
            this.txtIGV.PreventEnterBeep = true;
            this.txtIGV.ReadOnly = true;
            this.txtIGV.Size = new System.Drawing.Size(96, 22);
            this.txtIGV.TabIndex = 8;
            this.txtIGV.TextChanged += new System.EventHandler(this.txtIGV_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(202, 103);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Igv :";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // txtValorVenta
            // 
            this.txtValorVenta.BackColor = System.Drawing.Color.AliceBlue;
            // 
            // 
            // 
            this.txtValorVenta.Border.Class = "TextBoxBorder";
            this.txtValorVenta.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtValorVenta.Enabled = false;
            this.txtValorVenta.Location = new System.Drawing.Point(95, 101);
            this.txtValorVenta.Name = "txtValorVenta";
            this.txtValorVenta.PreventEnterBeep = true;
            this.txtValorVenta.ReadOnly = true;
            this.txtValorVenta.Size = new System.Drawing.Size(86, 22);
            this.txtValorVenta.TabIndex = 6;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnAnulaOV);
            this.groupBox6.Controls.Add(this.btnEditaOV);
            this.groupBox6.Controls.Add(this.btnInicioOV);
            this.groupBox6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox6.Location = new System.Drawing.Point(1077, 86);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(85, 19);
            this.groupBox6.TabIndex = 28;
            this.groupBox6.TabStop = false;
            this.groupBox6.Visible = false;
            // 
            // btnAnulaOV
            // 
            this.btnAnulaOV.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAnulaOV.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAnulaOV.Enabled = false;
            this.btnAnulaOV.Location = new System.Drawing.Point(269, 18);
            this.btnAnulaOV.Name = "btnAnulaOV";
            this.btnAnulaOV.Size = new System.Drawing.Size(75, 23);
            this.btnAnulaOV.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAnulaOV.TabIndex = 2;
            this.btnAnulaOV.Text = "Anula OV";
            this.btnAnulaOV.Click += new System.EventHandler(this.btnAnulaOV_Click);
            // 
            // btnEditaOV
            // 
            this.btnEditaOV.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEditaOV.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnEditaOV.Location = new System.Drawing.Point(153, 18);
            this.btnEditaOV.Name = "btnEditaOV";
            this.btnEditaOV.Size = new System.Drawing.Size(75, 23);
            this.btnEditaOV.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnEditaOV.TabIndex = 1;
            this.btnEditaOV.Text = "F4 Edita  OV";
            this.btnEditaOV.Click += new System.EventHandler(this.btnEditaOV_Click);
            // 
            // btnInicioOV
            // 
            this.btnInicioOV.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnInicioOV.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnInicioOV.Location = new System.Drawing.Point(33, 18);
            this.btnInicioOV.Margin = new System.Windows.Forms.Padding(0);
            this.btnInicioOV.Name = "btnInicioOV";
            this.btnInicioOV.Size = new System.Drawing.Size(75, 23);
            this.btnInicioOV.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnInicioOV.TabIndex = 0;
            this.btnInicioOV.Text = "F6 Inicia OV";
            this.btnInicioOV.Click += new System.EventHandler(this.btnInicioOV_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.BackColor = System.Drawing.Color.White;
            this.groupBox7.Controls.Add(this.chkVentaDsctoGlobal);
            this.groupBox7.Controls.Add(this.chkVentaGratuita);
            this.groupBox7.Controls.Add(this.txtBruto);
            this.groupBox7.Controls.Add(this.txtDscto);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.pbCapchatS);
            this.groupBox7.Controls.Add(this.txtSunat_Capchat);
            this.groupBox7.Controls.Add(this.txttasa);
            this.groupBox7.Controls.Add(this.label30);
            this.groupBox7.Controls.Add(this.lbLineaCredito);
            this.groupBox7.Controls.Add(this.txtLineaCredito);
            this.groupBox7.Controls.Add(this.txtLineaCreditoUso);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Controls.Add(this.label25);
            this.groupBox7.Controls.Add(this.txtLineaCreditoDisponible);
            this.groupBox7.Enabled = false;
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(905, 329);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(371, 109);
            this.groupBox7.TabIndex = 133;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Condiciones de Crédito:";
            this.groupBox7.Enter += new System.EventHandler(this.groupBox7_Enter);
            // 
            // chkVentaDsctoGlobal
            // 
            this.chkVentaDsctoGlobal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkVentaDsctoGlobal.AutoSize = true;
            // 
            // 
            // 
            this.chkVentaDsctoGlobal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkVentaDsctoGlobal.Location = new System.Drawing.Point(251, 38);
            this.chkVentaDsctoGlobal.Name = "chkVentaDsctoGlobal";
            this.chkVentaDsctoGlobal.Size = new System.Drawing.Size(100, 15);
            this.chkVentaDsctoGlobal.TabIndex = 131;
            this.chkVentaDsctoGlobal.Text = "Vta. Descuento";
            this.chkVentaDsctoGlobal.Visible = false;
            // 
            // chkVentaGratuita
            // 
            this.chkVentaGratuita.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkVentaGratuita.AutoSize = true;
            // 
            // 
            // 
            this.chkVentaGratuita.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkVentaGratuita.Location = new System.Drawing.Point(251, 17);
            this.chkVentaGratuita.Name = "chkVentaGratuita";
            this.chkVentaGratuita.Size = new System.Drawing.Size(87, 15);
            this.chkVentaGratuita.TabIndex = 130;
            this.chkVentaGratuita.Text = "Vta. Gratuita";
            this.chkVentaGratuita.Visible = false;
            // 
            // txtBruto
            // 
            this.txtBruto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBruto.Location = new System.Drawing.Point(263, 59);
            this.txtBruto.Name = "txtBruto";
            this.txtBruto.ReadOnly = true;
            this.txtBruto.Size = new System.Drawing.Size(62, 20);
            this.txtBruto.TabIndex = 126;
            this.txtBruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBruto.Visible = false;
            // 
            // txtDscto
            // 
            this.txtDscto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDscto.Location = new System.Drawing.Point(262, 82);
            this.txtDscto.Name = "txtDscto";
            this.txtDscto.ReadOnly = true;
            this.txtDscto.Size = new System.Drawing.Size(63, 20);
            this.txtDscto.TabIndex = 127;
            this.txtDscto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDscto.Visible = false;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(214, 85);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 13);
            this.label19.TabIndex = 129;
            this.label19.Text = "Dcto :";
            this.label19.Visible = false;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(210, 62);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 13);
            this.label20.TabIndex = 128;
            this.label20.Text = "Bruto :";
            this.label20.Visible = false;
            // 
            // pbCapchatS
            // 
            this.pbCapchatS.Location = new System.Drawing.Point(349, 55);
            this.pbCapchatS.Name = "pbCapchatS";
            this.pbCapchatS.Size = new System.Drawing.Size(19, 21);
            this.pbCapchatS.TabIndex = 123;
            this.pbCapchatS.TabStop = false;
            this.pbCapchatS.Visible = false;
            // 
            // txtSunat_Capchat
            // 
            this.txtSunat_Capchat.Location = new System.Drawing.Point(331, 56);
            this.txtSunat_Capchat.Name = "txtSunat_Capchat";
            this.txtSunat_Capchat.Size = new System.Drawing.Size(16, 20);
            this.txtSunat_Capchat.TabIndex = 122;
            this.txtSunat_Capchat.Visible = false;
            // 
            // txttasa
            // 
            this.txttasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttasa.Location = new System.Drawing.Point(104, 82);
            this.txttasa.Name = "txttasa";
            this.txttasa.ReadOnly = true;
            this.txttasa.Size = new System.Drawing.Size(88, 18);
            this.txttasa.TabIndex = 106;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(30, 85);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 12);
            this.label30.TabIndex = 105;
            this.label30.Text = "Tasa de Interés:";
            // 
            // lbLineaCredito
            // 
            this.lbLineaCredito.AutoSize = true;
            this.lbLineaCredito.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLineaCredito.Location = new System.Drawing.Point(7, 20);
            this.lbLineaCredito.Name = "lbLineaCredito";
            this.lbLineaCredito.Size = new System.Drawing.Size(94, 12);
            this.lbLineaCredito.TabIndex = 85;
            this.lbLineaCredito.Text = "Línea de Crédito (S/.):";
            // 
            // txtLineaCredito
            // 
            this.txtLineaCredito.Enabled = false;
            this.txtLineaCredito.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCredito.Location = new System.Drawing.Point(104, 17);
            this.txtLineaCredito.Name = "txtLineaCredito";
            this.txtLineaCredito.ReadOnly = true;
            this.txtLineaCredito.Size = new System.Drawing.Size(88, 18);
            this.txtLineaCredito.TabIndex = 84;
            // 
            // txtLineaCreditoUso
            // 
            this.txtLineaCreditoUso.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCreditoUso.Location = new System.Drawing.Point(104, 61);
            this.txtLineaCreditoUso.Name = "txtLineaCreditoUso";
            this.txtLineaCreditoUso.ReadOnly = true;
            this.txtLineaCreditoUso.Size = new System.Drawing.Size(88, 18);
            this.txtLineaCreditoUso.TabIndex = 98;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 40);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 12);
            this.label23.TabIndex = 95;
            this.label23.Text = "Línea Disponible (S/.):";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(8, 64);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(93, 12);
            this.label25.TabIndex = 97;
            this.label25.Text = "Línea C. en Uso (S/.):";
            // 
            // txtLineaCreditoDisponible
            // 
            this.txtLineaCreditoDisponible.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCreditoDisponible.Location = new System.Drawing.Point(104, 40);
            this.txtLineaCreditoDisponible.Name = "txtLineaCreditoDisponible";
            this.txtLineaCreditoDisponible.ReadOnly = true;
            this.txtLineaCreditoDisponible.Size = new System.Drawing.Size(88, 18);
            this.txtLineaCreditoDisponible.TabIndex = 96;
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.txtCodigoBarras);
            this.groupBox8.Controls.Add(this.chkVentaSinStock);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.lbDocumento);
            this.groupBox8.Controls.Add(this.txtDocRef);
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.txtPedido);
            this.groupBox8.Controls.Add(this.txtSerie);
            this.groupBox8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox8.Location = new System.Drawing.Point(643, 42);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(246, 53);
            this.groupBox8.TabIndex = 134;
            this.groupBox8.TabStop = false;
            // 
            // txtCodigoBarras
            // 
            this.txtCodigoBarras.BackColor = System.Drawing.Color.PeachPuff;
            // 
            // 
            // 
            this.txtCodigoBarras.Border.Class = "TextBoxBorder";
            this.txtCodigoBarras.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCodigoBarras.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoBarras.Location = new System.Drawing.Point(188, 16);
            this.txtCodigoBarras.Multiline = true;
            this.txtCodigoBarras.Name = "txtCodigoBarras";
            this.txtCodigoBarras.PreventEnterBeep = true;
            this.txtCodigoBarras.Size = new System.Drawing.Size(32, 23);
            this.txtCodigoBarras.TabIndex = 134;
            this.txtCodigoBarras.Text = "l";
            this.txtCodigoBarras.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCodigoBarras.Visible = false;
            // 
            // chkVentaSinStock
            // 
            this.chkVentaSinStock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkVentaSinStock.AutoSize = true;
            // 
            // 
            // 
            this.chkVentaSinStock.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkVentaSinStock.Location = new System.Drawing.Point(226, -62);
            this.chkVentaSinStock.Name = "chkVentaSinStock";
            this.chkVentaSinStock.Size = new System.Drawing.Size(94, 17);
            this.chkVentaSinStock.TabIndex = 133;
            this.chkVentaSinStock.Text = "Vta. Sin Stock";
            this.chkVentaSinStock.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(480, 17);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(15, 19);
            this.label21.TabIndex = 49;
            this.label21.Text = "-";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label21.Visible = false;
            // 
            // lbDocumento
            // 
            this.lbDocumento.AutoSize = true;
            this.lbDocumento.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDocumento.Location = new System.Drawing.Point(35, 14);
            this.lbDocumento.Name = "lbDocumento";
            this.lbDocumento.Size = new System.Drawing.Size(117, 25);
            this.lbDocumento.TabIndex = 48;
            this.lbDocumento.Tag = "22";
            this.lbDocumento.Text = "Documento";
            this.lbDocumento.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbDocumento.Visible = false;
            // 
            // txtDocRef
            // 
            this.txtDocRef.BackColor = System.Drawing.Color.PeachPuff;
            // 
            // 
            // 
            this.txtDocRef.Border.Class = "TextBoxBorder";
            this.txtDocRef.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDocRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocRef.Location = new System.Drawing.Point(64, 14);
            this.txtDocRef.Multiline = true;
            this.txtDocRef.Name = "txtDocRef";
            this.txtDocRef.PreventEnterBeep = true;
            this.txtDocRef.Size = new System.Drawing.Size(32, 23);
            this.txtDocRef.TabIndex = 47;
            this.txtDocRef.Text = "L";
            this.txtDocRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDocRef.Visible = false;
            this.txtDocRef.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocRef_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 46;
            this.label12.Text = "Doc. Ref.";
            this.label12.Visible = false;
            // 
            // txtPedido
            // 
            // 
            // 
            // 
            this.txtPedido.Border.Class = "TextBoxBorder";
            this.txtPedido.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPedido.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPedido.Location = new System.Drawing.Point(501, 14);
            this.txtPedido.Multiline = true;
            this.txtPedido.Name = "txtPedido";
            this.txtPedido.PreventEnterBeep = true;
            this.txtPedido.Size = new System.Drawing.Size(117, 24);
            this.txtPedido.TabIndex = 45;
            this.txtPedido.Text = "00000000";
            this.txtPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSerie
            // 
            // 
            // 
            // 
            this.txtSerie.Border.Class = "TextBoxBorder";
            this.txtSerie.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSerie.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerie.Location = new System.Drawing.Point(401, 14);
            this.txtSerie.Multiline = true;
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.PreventEnterBeep = true;
            this.txtSerie.Size = new System.Drawing.Size(68, 23);
            this.txtSerie.TabIndex = 0;
            this.txtSerie.Text = "000";
            this.txtSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSerie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSerie_KeyDown);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(161, 20);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(15, 19);
            this.label22.TabIndex = 140;
            this.label22.Text = "-";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxX1
            // 
            // 
            // 
            // 
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX1.Location = new System.Drawing.Point(182, 17);
            this.textBoxX1.Multiline = true;
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.PreventEnterBeep = true;
            this.textBoxX1.Size = new System.Drawing.Size(117, 30);
            this.textBoxX1.TabIndex = 139;
            this.textBoxX1.Text = "00000000";
            this.textBoxX1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxX2
            // 
            // 
            // 
            // 
            this.textBoxX2.Border.Class = "TextBoxBorder";
            this.textBoxX2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX2.Location = new System.Drawing.Point(82, 17);
            this.textBoxX2.Multiline = true;
            this.textBoxX2.Name = "textBoxX2";
            this.textBoxX2.PreventEnterBeep = true;
            this.textBoxX2.Size = new System.Drawing.Size(68, 30);
            this.textBoxX2.TabIndex = 138;
            this.textBoxX2.Text = "000";
            this.textBoxX2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.textBoxX5);
            this.groupBox9.Controls.Add(this.textBoxX1);
            this.groupBox9.Controls.Add(this.textBoxX2);
            this.groupBox9.Controls.Add(this.textBoxX6);
            this.groupBox9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox9.Location = new System.Drawing.Point(905, 40);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(371, 53);
            this.groupBox9.TabIndex = 141;
            this.groupBox9.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(480, 17);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(15, 19);
            this.label24.TabIndex = 49;
            this.label24.Text = "-";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label24.Visible = false;
            // 
            // textBoxX5
            // 
            // 
            // 
            // 
            this.textBoxX5.Border.Class = "TextBoxBorder";
            this.textBoxX5.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX5.Location = new System.Drawing.Point(501, 14);
            this.textBoxX5.Multiline = true;
            this.textBoxX5.Name = "textBoxX5";
            this.textBoxX5.PreventEnterBeep = true;
            this.textBoxX5.Size = new System.Drawing.Size(117, 24);
            this.textBoxX5.TabIndex = 45;
            this.textBoxX5.Text = "00000000";
            this.textBoxX5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxX6
            // 
            // 
            // 
            // 
            this.textBoxX6.Border.Class = "TextBoxBorder";
            this.textBoxX6.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX6.Location = new System.Drawing.Point(401, 14);
            this.textBoxX6.Multiline = true;
            this.textBoxX6.Name = "textBoxX6";
            this.textBoxX6.PreventEnterBeep = true;
            this.textBoxX6.Size = new System.Drawing.Size(68, 23);
            this.textBoxX6.TabIndex = 0;
            this.textBoxX6.Text = "000";
            this.textBoxX6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // requiredFieldValidator1
            // 
            this.requiredFieldValidator1.ErrorMessage = "Seleccionar Vendedor";
            this.requiredFieldValidator1.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // dgvStockAlmacenes
            // 
            this.dgvStockAlmacenes.AllowUserToAddRows = false;
            this.dgvStockAlmacenes.AllowUserToDeleteRows = false;
            this.dgvStockAlmacenes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(124)))), ((int)(((byte)(210)))));
            dataGridViewCellStyle51.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle51.ForeColor = System.Drawing.SystemColors.Menu;
            dataGridViewCellStyle51.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle51.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle51.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStockAlmacenes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle51;
            this.dgvStockAlmacenes.ColumnHeadersHeight = 26;
            this.dgvStockAlmacenes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idalmacen,
            this.idproductoalmacen,
            this.nomempresa,
            this.nomalmacen,
            this.stockalmacen});
            this.dgvStockAlmacenes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvStockAlmacenes.EnableHeadersVisualStyles = false;
            this.dgvStockAlmacenes.Location = new System.Drawing.Point(9, 18);
            this.dgvStockAlmacenes.Name = "dgvStockAlmacenes";
            this.dgvStockAlmacenes.ReadOnly = true;
            this.dgvStockAlmacenes.RowHeadersVisible = false;
            this.dgvStockAlmacenes.Size = new System.Drawing.Size(443, 110);
            this.dgvStockAlmacenes.TabIndex = 146;
            // 
            // idalmacen
            // 
            this.idalmacen.DataPropertyName = "idalmacen";
            this.idalmacen.HeaderText = "idalmacen";
            this.idalmacen.Name = "idalmacen";
            this.idalmacen.ReadOnly = true;
            this.idalmacen.Visible = false;
            // 
            // idproductoalmacen
            // 
            this.idproductoalmacen.DataPropertyName = "idproductoalmacen";
            this.idproductoalmacen.HeaderText = "idproductoalmacen";
            this.idproductoalmacen.Name = "idproductoalmacen";
            this.idproductoalmacen.ReadOnly = true;
            this.idproductoalmacen.Visible = false;
            // 
            // nomempresa
            // 
            this.nomempresa.DataPropertyName = "nomempresa";
            this.nomempresa.HeaderText = "Empresa";
            this.nomempresa.Name = "nomempresa";
            this.nomempresa.ReadOnly = true;
            // 
            // nomalmacen
            // 
            this.nomalmacen.DataPropertyName = "nomalmacen";
            this.nomalmacen.HeaderText = "Almacen";
            this.nomalmacen.Name = "nomalmacen";
            this.nomalmacen.ReadOnly = true;
            this.nomalmacen.Width = 150;
            // 
            // stockalmacen
            // 
            this.stockalmacen.DataPropertyName = "stockalmacen";
            this.stockalmacen.HeaderText = "Stock";
            this.stockalmacen.Name = "stockalmacen";
            this.stockalmacen.ReadOnly = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.dgvStockAlmacenes);
            this.groupBox10.Controls.Add(this.label1);
            this.groupBox10.Controls.Add(this.cmbAlmacenes);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(247, 285);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(461, 141);
            this.groupBox10.TabIndex = 147;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "DETALLE PRODUCTO";
            this.groupBox10.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(592, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 148;
            this.label1.Text = "Seleccion de almacen:";
            this.label1.Visible = false;
            // 
            // cmbAlmacenes
            // 
            this.cmbAlmacenes.Enabled = false;
            this.cmbAlmacenes.FormattingEnabled = true;
            this.cmbAlmacenes.Location = new System.Drawing.Point(525, 44);
            this.cmbAlmacenes.Name = "cmbAlmacenes";
            this.cmbAlmacenes.Size = new System.Drawing.Size(310, 21);
            this.cmbAlmacenes.TabIndex = 147;
            this.cmbAlmacenes.Visible = false;
            this.cmbAlmacenes.SelectionChangeCommitted += new System.EventHandler(this.cmbAlmacenes_SelectionChangeCommitted);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 24);
            // 
            // tsNoimpre
            // 
            this.tsNoimpre.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsNoimpre.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.tsNoimpre.Name = "tsNoimpre";
            this.tsNoimpre.Size = new System.Drawing.Size(105, 20);
            this.tsNoimpre.Text = "Guardar/No Impr.";
            this.tsNoimpre.ToolTipText = "Guardar/No Impr.";
            this.tsNoimpre.Visible = false;
            this.tsNoimpre.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // tsNoImpr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CaptionAntiAlias = false;
            this.CaptionFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientSize = new System.Drawing.Size(1299, 622);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.groupBox10);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "tsNoImpr";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ORDEN DE VENTA";
            this.Load += new System.EventHandler(this.frmVenta2019_Load);
            this.Shown += new System.EventHandler(this.frmVenta2019_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVenta2019_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmVenta2019_KeyUp);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvproductos)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvdetalle)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMoneda)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCapchatS)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockAlmacenes)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx panel;
        private DevComponents.DotNetBar.Controls.Line line2;
        private System.Windows.Forms.ToolStripButton toolStripGuardar;
        private System.Windows.Forms.ToolStripButton toolStripButtonSalir;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnRefrescar;
        private System.Windows.Forms.DataGridView dgvproductos;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDireccion;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNombreCliente;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpFecha1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPrecioVenta;
        private System.Windows.Forms.Label label9;
        private DevComponents.DotNetBar.Controls.TextBoxX txtIGV;
        private System.Windows.Forms.Label label8;
        private DevComponents.DotNetBar.Controls.TextBoxX txtValorVenta;
   
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelx;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.DataGridView dgvdetalle;
        private System.Windows.Forms.RadioButton chkFactura;
        private System.Windows.Forms.RadioButton chkBoleta;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label5;
        private DevComponents.DotNetBar.Controls.TextBoxX txtinafectas;
        private DevComponents.DotNetBar.Controls.TextBoxX txtgratuitas;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txttasa;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lbLineaCredito;
        private System.Windows.Forms.TextBox txtLineaCredito;
        private System.Windows.Forms.TextBox txtLineaCreditoUso;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtLineaCreditoDisponible;
        public DevComponents.DotNetBar.Controls.TextBoxX txtCodCliente;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbFormaPago;
        private System.Windows.Forms.DateTimePicker dtpFechaPago;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodigoVendedor;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNombreVendedor;
        private DevComponents.DotNetBar.ButtonX btnInicioOV;
        private System.Windows.Forms.GroupBox groupBox8;
        private DevComponents.DotNetBar.ButtonX btnAnulaOV;
        private DevComponents.DotNetBar.ButtonX btnEditaOV;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSerie;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPedido;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDocRef;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbDocumento;
        private System.Windows.Forms.Label label21;
        public DevComponents.DotNetBar.Controls.CheckBoxX chkVentaSinStock;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCodigoBarras;
        private System.Windows.Forms.PictureBox pbCapchatS;
        private System.Windows.Forms.TextBox txtSunat_Capchat;
        private System.Windows.Forms.TextBox txtBruto;
        private System.Windows.Forms.TextBox txtDscto;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX2;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox9;
        private DevComponents.DotNetBar.Validator.SuperValidator superValidator1;
        public DevComponents.DotNetBar.Controls.CheckBoxX chkVentaDsctoGlobal;
        public DevComponents.DotNetBar.Controls.CheckBoxX chkVentaGratuita;
        private DevComponents.DotNetBar.Controls.TextBoxX txtexoneradas;
        private DevComponents.DotNetBar.Controls.TextBoxX txtgravadas;
        private System.Windows.Forms.Label label24;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX5;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX6;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripIniciaov;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripEditaov;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripAnulaov;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.DataGridView dgvStockAlmacenes;
        private System.Windows.Forms.DataGridViewTextBoxColumn idalmacen;
        private System.Windows.Forms.DataGridViewTextBoxColumn idproductoalmacen;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomempresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomalmacen;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockalmacen;
        private System.Windows.Forms.GroupBox groupBox10;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator1;
        private System.Windows.Forms.RadioButton chkTicket;
        private System.Windows.Forms.ToolStripButton toolStripButtonPendiente;
        private System.Windows.Forms.ToolStripButton toolStripImprimir;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbAlmacenes;
        private System.Windows.Forms.DataGridViewTextBoxColumn coddetalle;
        private System.Windows.Forms.DataGridViewTextBoxColumn codproducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn referenci;
        private System.Windows.Forms.DataGridViewTextBoxColumn product;
        private System.Windows.Forms.DataGridViewTextBoxColumn codunidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn unida;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn preciounit;
        private System.Windows.Forms.DataGridViewTextBoxColumn importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto3;
        private System.Windows.Forms.DataGridViewTextBoxColumn montodscto;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorventa;
        private System.Windows.Forms.DataGridViewTextBoxColumn igv;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioventa;
        private System.Windows.Forms.DataGridViewTextBoxColumn valoreal;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioreal;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioconigv;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorpromedio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipoarticulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipoimpuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn codalmacen;
        private System.Windows.Forms.DataGridViewTextBoxColumn almacen;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoUnidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn empres;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private DevComponents.DotNetBar.LabelX labelX7;
        private Telerik.WinControls.UI.RadDropDownList cmbMoneda;
        private Telerik.WinControls.Themes.TelerikMetroBlueTheme telerikMetroBlueTheme1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn codUniversal;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomAlma;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcion;
        private System.Windows.Forms.DataGridViewComboBoxColumn unidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn nmarca;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockdisponible;
        private System.Windows.Forms.DataGridViewTextBoxColumn cant;
        private System.Windows.Forms.DataGridViewTextBoxColumn precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.DataGridViewTextBoxColumn codunidadmedida;
        private System.Windows.Forms.DataGridViewTextBoxColumn codsunatimpuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn codtimpuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn codalma;
        private System.Windows.Forms.DataGridViewTextBoxColumn unidadnombre;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton tsNoimpre;
    }
}