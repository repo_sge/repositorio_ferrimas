﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SIGEFA.Administradores;

namespace SIGEFA.Formularios
{
    public partial class frmProductoSunat : Form
    {
        clsAdmProducto admPro = new clsAdmProducto();
        List<DataTable> listatablas = new List<DataTable>();
        DataTable databi = new DataTable();

        public frmProductoSunat()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        public String GetCodigoSunat()
        {
            return dgvProductoSunat.CurrentRow.Cells[codigosunat.Name].Value.ToString();
        }

        private void txtfiltro_TextChanged(object sender, EventArgs e)
        {
            try
            {
                databi = admPro.CargaProductoSunat();
                if (txtfiltro.Text.Length >= 2)
                {
                    listatablas.Clear(); //listatablas01.Clear();
                    String cad = txtfiltro.Text;
                    String[] AUnidad = cad.Split(' ');
                    DataTable datamel = null;
                    for (int i = 0; i < AUnidad.Length; i++)
                    {
                        datamel = null;
                        if (i == 0)
                        {
                            datamel = databi.AsEnumerable()
                                     .Where(r => r.Field<string>("descripcionproducto").Contains(AUnidad[i].Trim()))
                                     .CopyToDataTable();
                            listatablas.Add(datamel);

                        }
                        else
                        {
                            for (int j = 0; j < listatablas.Count; j++)
                            {
                                if (listatablas.Count <= i)
                                {
                                    if (listatablas[j].AsEnumerable()
                                            .Where(r => r.Field<string>("descripcionproducto").Contains(AUnidad[i].Trim())).Any())
                                    {
                                        datamel = listatablas[j].AsEnumerable()
                                                .Where(r => r.Field<string>("descripcionproducto").Contains(AUnidad[i].Trim()))
                                                .CopyToDataTable();
                                        listatablas.Add(datamel);
                                        break;
                                    }
                                }
                            }
                        }

                        if (datamel != null)
                        {
                            dgvProductoSunat.Rows.Clear();
                            foreach (DataRow row in datamel.Rows)
                            {
                                dgvProductoSunat.Rows.Add(row[2].ToString(), row[3].ToString());
                            }
                        }

                    }
                }
                else
                {
                    dgvProductoSunat.Rows.Clear();
                    foreach (DataRow row in databi.Rows)
                    {
                        dgvProductoSunat.Rows.Add(row[2].ToString(), row[3].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvProductoSunat_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DialogResult = DialogResult.OK; this.Close();
        }
    }
}
