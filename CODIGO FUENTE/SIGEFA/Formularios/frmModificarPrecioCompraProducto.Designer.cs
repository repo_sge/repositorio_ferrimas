﻿namespace SIGEFA.Formularios
{
	partial class frmModificarPrecioCompraProducto
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// frmModificarPrecioCompraProducto
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(495, 178);
			this.DoubleBuffered = true;
			this.Name = "frmModificarPrecioCompraProducto";
			this.Text = "CAMBIO EN EL PRECIO DE COMPRA DE PRODUCTO";
			this.Load += new System.EventHandler(this.frmModificarPrecioCompraProducto_Load);
			this.ResumeLayout(false);

		}

		#endregion
	}
}