﻿namespace SIGEFA.Formularios
{
    partial class frmDetalleSalida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDetalleSalida));
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.txtStockMinimo = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.txtUbicacion = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.txtReferencia = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtDescripcion = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtStock = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtCantidad = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.txtPrecio = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.cmbUnidad = new System.Windows.Forms.ComboBox();
			this.txtDscto1 = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.txtUltimoPrecioCompra = new System.Windows.Forms.TextBox();
			this.txtPrecioNeto = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.txtPrecioNetoDscto = new System.Windows.Forms.TextBox();
			this.txtDscto2 = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.txtPrecioDscto = new System.Windows.Forms.TextBox();
			this.txtDscto3 = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.txtUnidad = new System.Windows.Forms.TextBox();
			this.txtControlStock = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtCodigo = new System.Windows.Forms.TextBox();
			this.txtUnd = new System.Windows.Forms.TextBox();
			this.txtPrecioNetoDolares = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.txtPrecioMax = new System.Windows.Forms.TextBox();
			this.btnSalir = new System.Windows.Forms.Button();
			this.btnGuardar = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "Write Document.png");
			this.imageList1.Images.SetKeyName(1, "New Document.png");
			this.imageList1.Images.SetKeyName(2, "Remove Document.png");
			this.imageList1.Images.SetKeyName(3, "document-print.png");
			this.imageList1.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
			this.imageList1.Images.SetKeyName(5, "exit.png");
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.panel1);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(12, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(845, 190);
			this.groupBox1.TabIndex = 15;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Ingresar Artículo";
			// 
			// panel1
			// 
			this.panel1.AutoScroll = true;
			this.panel1.Controls.Add(this.txtStockMinimo);
			this.panel1.Controls.Add(this.label20);
			this.panel1.Controls.Add(this.txtUbicacion);
			this.panel1.Controls.Add(this.label15);
			this.panel1.Controls.Add(this.txtReferencia);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.txtDescripcion);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.txtStock);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.txtCantidad);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.txtPrecio);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.cmbUnidad);
			this.panel1.Controls.Add(this.txtDscto1);
			this.panel1.Controls.Add(this.label12);
			this.panel1.Controls.Add(this.label9);
			this.panel1.Controls.Add(this.txtUltimoPrecioCompra);
			this.panel1.Controls.Add(this.txtPrecioNeto);
			this.panel1.Controls.Add(this.label10);
			this.panel1.Controls.Add(this.txtPrecioNetoDscto);
			this.panel1.Controls.Add(this.txtDscto2);
			this.panel1.Controls.Add(this.label11);
			this.panel1.Controls.Add(this.txtPrecioDscto);
			this.panel1.Controls.Add(this.txtDscto3);
			this.panel1.Location = new System.Drawing.Point(6, 19);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(833, 165);
			this.panel1.TabIndex = 38;
			// 
			// txtStockMinimo
			// 
			this.txtStockMinimo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtStockMinimo.Location = new System.Drawing.Point(314, 135);
			this.txtStockMinimo.Name = "txtStockMinimo";
			this.txtStockMinimo.ReadOnly = true;
			this.txtStockMinimo.Size = new System.Drawing.Size(177, 22);
			this.txtStockMinimo.TabIndex = 34;
			this.txtStockMinimo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label20.Location = new System.Drawing.Point(311, 116);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(104, 16);
			this.label20.TabIndex = 33;
			this.label20.Text = "Stock Mínimo:";
			// 
			// txtUbicacion
			// 
			this.txtUbicacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtUbicacion.Location = new System.Drawing.Point(137, 135);
			this.txtUbicacion.Name = "txtUbicacion";
			this.txtUbicacion.ReadOnly = true;
			this.txtUbicacion.Size = new System.Drawing.Size(171, 22);
			this.txtUbicacion.TabIndex = 28;
			this.txtUbicacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.Location = new System.Drawing.Point(137, 116);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(86, 16);
			this.label15.TabIndex = 27;
			this.label15.Text = "Ubicación :";
			// 
			// txtReferencia
			// 
			this.txtReferencia.BackColor = System.Drawing.Color.PeachPuff;
			this.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtReferencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtReferencia.Location = new System.Drawing.Point(7, 29);
			this.txtReferencia.Name = "txtReferencia";
			this.txtReferencia.Size = new System.Drawing.Size(142, 22);
			this.txtReferencia.TabIndex = 1;
			this.txtReferencia.TextChanged += new System.EventHandler(this.frmDetalleSalida_Load);
			this.txtReferencia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReferencia_KeyDown);
			this.txtReferencia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReferencia_KeyPress);
			this.txtReferencia.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtReferencia_KeyUp);
			this.txtReferencia.Leave += new System.EventHandler(this.txtReferencia_Leave);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(4, 10);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(92, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Referencia :";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(153, 10);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(99, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "Descripcion :";
			// 
			// txtDescripcion
			// 
			this.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtDescripcion.Enabled = false;
			this.txtDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtDescripcion.Location = new System.Drawing.Point(155, 29);
			this.txtDescripcion.Name = "txtDescripcion";
			this.txtDescripcion.ReadOnly = true;
			this.txtDescripcion.Size = new System.Drawing.Size(522, 22);
			this.txtDescripcion.TabIndex = 2;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(683, 10);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(134, 16);
			this.label4.TabIndex = 6;
			this.label4.Text = "Stock Disponible :";
			// 
			// txtStock
			// 
			this.txtStock.Enabled = false;
			this.txtStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtStock.Location = new System.Drawing.Point(686, 29);
			this.txtStock.Name = "txtStock";
			this.txtStock.Size = new System.Drawing.Size(139, 22);
			this.txtStock.TabIndex = 3;
			this.txtStock.TextChanged += new System.EventHandler(this.txtStock_TextChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(225, 61);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(78, 16);
			this.label5.TabIndex = 8;
			this.label5.Text = "Cantidad :";
			// 
			// txtCantidad
			// 
			this.txtCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtCantidad.Location = new System.Drawing.Point(228, 80);
			this.txtCantidad.Name = "txtCantidad";
			this.txtCantidad.Size = new System.Drawing.Size(80, 22);
			this.txtCantidad.TabIndex = 6;
			this.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCantidad_KeyPress);
			this.txtCantidad.Leave += new System.EventHandler(this.txtCantidad_Leave);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(4, 61);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(66, 16);
			this.label6.TabIndex = 10;
			this.label6.Text = "Unidad :";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(311, 61);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(88, 16);
			this.label8.TabIndex = 12;
			this.label8.Text = "Precio Unit:";
			// 
			// txtPrecio
			// 
			this.txtPrecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtPrecio.Location = new System.Drawing.Point(314, 80);
			this.txtPrecio.Name = "txtPrecio";
			this.txtPrecio.Size = new System.Drawing.Size(91, 22);
			this.txtPrecio.TabIndex = 7;
			this.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtPrecio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrecio_KeyDown);
			this.txtPrecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecio_KeyPress);
			this.txtPrecio.Leave += new System.EventHandler(this.txtPrecio_Leave);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(408, 61);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(83, 16);
			this.label7.TabIndex = 14;
			this.label7.Text = " Dscto S/. :";
			this.label7.Visible = false;
			// 
			// cmbUnidad
			// 
			this.cmbUnidad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.cmbUnidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbUnidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbUnidad.FormattingEnabled = true;
			this.cmbUnidad.Location = new System.Drawing.Point(7, 80);
			this.cmbUnidad.Name = "cmbUnidad";
			this.cmbUnidad.Size = new System.Drawing.Size(215, 24);
			this.cmbUnidad.TabIndex = 19;
			this.cmbUnidad.SelectionChangeCommitted += new System.EventHandler(this.cmbUnidad_SelectionChangeCommitted);
			// 
			// txtDscto1
			// 
			this.txtDscto1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtDscto1.Location = new System.Drawing.Point(411, 80);
			this.txtDscto1.MaxLength = 4;
			this.txtDscto1.Name = "txtDscto1";
			this.txtDscto1.Size = new System.Drawing.Size(80, 22);
			this.txtDscto1.TabIndex = 8;
			this.txtDscto1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDscto1.Visible = false;
			this.txtDscto1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDscto1_KeyDown);
			this.txtDscto1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDscto1_KeyPress);
			this.txtDscto1.Leave += new System.EventHandler(this.txtDscto1_Leave);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.Location = new System.Drawing.Point(4, 116);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(127, 16);
			this.label12.TabIndex = 22;
			this.label12.Text = "Últim. P. Compra:";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(683, 61);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(97, 16);
			this.label9.TabIndex = 16;
			this.label9.Text = "Precio Total:";
			// 
			// txtUltimoPrecioCompra
			// 
			this.txtUltimoPrecioCompra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtUltimoPrecioCompra.Location = new System.Drawing.Point(7, 135);
			this.txtUltimoPrecioCompra.Name = "txtUltimoPrecioCompra";
			this.txtUltimoPrecioCompra.ReadOnly = true;
			this.txtUltimoPrecioCompra.Size = new System.Drawing.Size(124, 22);
			this.txtUltimoPrecioCompra.TabIndex = 26;
			this.txtUltimoPrecioCompra.TabStop = false;
			this.txtUltimoPrecioCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtPrecioNeto
			// 
			this.txtPrecioNeto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtPrecioNeto.Location = new System.Drawing.Point(686, 80);
			this.txtPrecioNeto.Name = "txtPrecioNeto";
			this.txtPrecioNeto.ReadOnly = true;
			this.txtPrecioNeto.Size = new System.Drawing.Size(139, 22);
			this.txtPrecioNeto.TabIndex = 11;
			this.txtPrecioNeto.TabStop = false;
			this.txtPrecioNeto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtPrecioNeto.TextChanged += new System.EventHandler(this.txtPrecioNeto_TextChanged);
			this.txtPrecioNeto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecioNeto_KeyPress);
			this.txtPrecioNeto.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPrecioNeto_KeyUp);
			this.txtPrecioNeto.Leave += new System.EventHandler(this.txtPrecioNeto_Leave);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(499, 61);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(90, 16);
			this.label10.TabIndex = 18;
			this.label10.Text = "Aument S./ :";
			this.label10.Visible = false;
			// 
			// txtPrecioNetoDscto
			// 
			this.txtPrecioNetoDscto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtPrecioNetoDscto.Location = new System.Drawing.Point(731, 80);
			this.txtPrecioNetoDscto.Name = "txtPrecioNetoDscto";
			this.txtPrecioNetoDscto.ReadOnly = true;
			this.txtPrecioNetoDscto.Size = new System.Drawing.Size(90, 22);
			this.txtPrecioNetoDscto.TabIndex = 24;
			this.txtPrecioNetoDscto.TabStop = false;
			this.txtPrecioNetoDscto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtPrecioNetoDscto.Visible = false;
			// 
			// txtDscto2
			// 
			this.txtDscto2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtDscto2.Location = new System.Drawing.Point(502, 80);
			this.txtDscto2.Name = "txtDscto2";
			this.txtDscto2.Size = new System.Drawing.Size(87, 22);
			this.txtDscto2.TabIndex = 9;
			this.txtDscto2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDscto2.Visible = false;
			this.txtDscto2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDscto2_KeyDown);
			this.txtDscto2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDscto2_KeyPress);
			this.txtDscto2.Leave += new System.EventHandler(this.txtDscto2_Leave);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.Location = new System.Drawing.Point(595, 61);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(85, 16);
			this.label11.TabIndex = 20;
			this.label11.Text = "% Dscto 3 :";
			this.label11.Visible = false;
			// 
			// txtPrecioDscto
			// 
			this.txtPrecioDscto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtPrecioDscto.Location = new System.Drawing.Point(228, 80);
			this.txtPrecioDscto.Name = "txtPrecioDscto";
			this.txtPrecioDscto.ReadOnly = true;
			this.txtPrecioDscto.Size = new System.Drawing.Size(80, 22);
			this.txtPrecioDscto.TabIndex = 21;
			this.txtPrecioDscto.TabStop = false;
			this.txtPrecioDscto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtPrecioDscto.Visible = false;
			// 
			// txtDscto3
			// 
			this.txtDscto3.Enabled = false;
			this.txtDscto3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtDscto3.Location = new System.Drawing.Point(598, 80);
			this.txtDscto3.Name = "txtDscto3";
			this.txtDscto3.Size = new System.Drawing.Size(82, 22);
			this.txtDscto3.TabIndex = 10;
			this.txtDscto3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.txtDscto3.Visible = false;
			this.txtDscto3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDscto3_KeyPress);
			this.txtDscto3.Leave += new System.EventHandler(this.txtDscto3_Leave);
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(396, 199);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(72, 13);
			this.label14.TabIndex = 27;
			this.label14.Text = "% Max Dscto.";
			this.label14.Visible = false;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(570, 199);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(88, 13);
			this.label13.TabIndex = 23;
			this.label13.Text = "Max Dscto Total:";
			this.label13.Visible = false;
			// 
			// txtUnidad
			// 
			this.txtUnidad.Enabled = false;
			this.txtUnidad.Location = new System.Drawing.Point(391, 215);
			this.txtUnidad.Name = "txtUnidad";
			this.txtUnidad.Size = new System.Drawing.Size(80, 20);
			this.txtUnidad.TabIndex = 4;
			this.txtUnidad.Visible = false;
			// 
			// txtControlStock
			// 
			this.txtControlStock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtControlStock.Location = new System.Drawing.Point(303, 215);
			this.txtControlStock.Name = "txtControlStock";
			this.txtControlStock.Size = new System.Drawing.Size(80, 20);
			this.txtControlStock.TabIndex = 5;
			this.txtControlStock.Visible = false;
			this.txtControlStock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtControlStock_KeyPress);
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(314, 199);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(69, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Serie / Lote :";
			this.label3.Visible = false;
			// 
			// txtCodigo
			// 
			this.txtCodigo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtCodigo.Location = new System.Drawing.Point(217, 215);
			this.txtCodigo.Name = "txtCodigo";
			this.txtCodigo.Size = new System.Drawing.Size(80, 20);
			this.txtCodigo.TabIndex = 16;
			this.txtCodigo.Visible = false;
			this.txtCodigo.TextChanged += new System.EventHandler(this.txtCodigo_TextChanged);
			this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigo_KeyPress);
			// 
			// txtUnd
			// 
			this.txtUnd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtUnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtUnd.Location = new System.Drawing.Point(573, 215);
			this.txtUnd.Name = "txtUnd";
			this.txtUnd.Size = new System.Drawing.Size(84, 20);
			this.txtUnd.TabIndex = 20;
			this.txtUnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtUnd.Visible = false;
			// 
			// txtPrecioNetoDolares
			// 
			this.txtPrecioNetoDolares.Location = new System.Drawing.Point(477, 215);
			this.txtPrecioNetoDolares.Name = "txtPrecioNetoDolares";
			this.txtPrecioNetoDolares.Size = new System.Drawing.Size(90, 20);
			this.txtPrecioNetoDolares.TabIndex = 21;
			this.txtPrecioNetoDolares.Visible = false;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(474, 199);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(81, 13);
			this.label16.TabIndex = 29;
			this.label16.Text = "Precio Total $/.";
			this.label16.Visible = false;
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.checkBox1.Location = new System.Drawing.Point(18, 215);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(266, 17);
			this.checkBox1.TabIndex = 30;
			this.checkBox1.Text = "AUMENTO SOBRE EL PRECIO UNITARIO";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.Visible = false;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// checkBox2
			// 
			this.checkBox2.AutoSize = true;
			this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.checkBox2.Location = new System.Drawing.Point(18, 196);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(281, 17);
			this.checkBox2.TabIndex = 31;
			this.checkBox2.Text = "DESCUENTO SOBRE EL PRECIO UNITARIO";
			this.checkBox2.UseVisualStyleBackColor = true;
			this.checkBox2.Visible = false;
			this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
			// 
			// txtPrecioMax
			// 
			this.txtPrecioMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtPrecioMax.Location = new System.Drawing.Point(119, 213);
			this.txtPrecioMax.Name = "txtPrecioMax";
			this.txtPrecioMax.ReadOnly = true;
			this.txtPrecioMax.Size = new System.Drawing.Size(69, 22);
			this.txtPrecioMax.TabIndex = 25;
			this.txtPrecioMax.TabStop = false;
			this.txtPrecioMax.Visible = false;
			// 
			// btnSalir
			// 
			this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
			this.btnSalir.Location = new System.Drawing.Point(734, 206);
			this.btnSalir.Name = "btnSalir";
			this.btnSalir.Size = new System.Drawing.Size(123, 32);
			this.btnSalir.TabIndex = 18;
			this.btnSalir.Text = "CANCELAR";
			this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnSalir.UseVisualStyleBackColor = true;
			this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
			// 
			// btnGuardar
			// 
			this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnGuardar.Enabled = false;
			this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
			this.btnGuardar.Location = new System.Drawing.Point(548, 206);
			this.btnGuardar.Name = "btnGuardar";
			this.btnGuardar.Size = new System.Drawing.Size(180, 32);
			this.btnGuardar.TabIndex = 17;
			this.btnGuardar.Text = "AGREGAR A LISTA";
			this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnGuardar.UseVisualStyleBackColor = true;
			this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
			// 
			// frmDetalleSalida
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnSalir;
			this.ClientSize = new System.Drawing.Size(869, 250);
			this.Controls.Add(this.checkBox2);
			this.Controls.Add(this.checkBox1);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.btnSalir);
			this.Controls.Add(this.btnGuardar);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.txtCodigo);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtControlStock);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.txtUnidad);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.txtPrecioMax);
			this.Controls.Add(this.txtUnd);
			this.Controls.Add(this.txtPrecioNetoDolares);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmDetalleSalida";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Detalle Salida";
			this.Load += new System.EventHandler(this.frmDetalleSalida_Load);
			this.Shown += new System.EventHandler(this.frmDetalleSalida_Shown);
			this.groupBox1.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox txtReferencia;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStock;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtCodigo;
        public System.Windows.Forms.TextBox txtDscto3;
        public System.Windows.Forms.TextBox txtDscto2;
        public System.Windows.Forms.TextBox txtPrecioNeto;
        public System.Windows.Forms.TextBox txtDscto1;
        public System.Windows.Forms.TextBox txtPrecio;
        public System.Windows.Forms.TextBox txtCantidad;
        public System.Windows.Forms.TextBox txtControlStock;
        public System.Windows.Forms.TextBox txtUnidad;
        public System.Windows.Forms.TextBox txtPrecioDscto;
        public System.Windows.Forms.TextBox txtUltimoPrecioCompra;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox txtPrecioNetoDscto;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox txtDescripcion;
        public System.Windows.Forms.TextBox txtUnd;
        public System.Windows.Forms.ComboBox cmbUnidad;
        public System.Windows.Forms.TextBox txtPrecioNetoDolares;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TextBox txtUbicacion;
		private System.Windows.Forms.Label label15;
		public System.Windows.Forms.TextBox txtPrecioMax;
		private System.Windows.Forms.TextBox txtStockMinimo;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label12;
	}
}