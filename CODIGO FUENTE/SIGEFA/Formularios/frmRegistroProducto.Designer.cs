﻿namespace SIGEFA.Formularios
{
    partial class frmRegistroProducto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistroProducto));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ckbVentaTicket = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.txtStockMinimo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtUbicacion = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCodigoUniversal = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_procentaje_retencion = new System.Windows.Forms.TextBox();
            this.lb_procentaje_retencion = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.rdtExonerado = new System.Windows.Forms.RadioButton();
            this.rdtInafecto = new System.Windows.Forms.RadioButton();
            this.rdtGravado = new System.Windows.Forms.RadioButton();
            this.lbPrecioVenta = new System.Windows.Forms.Label();
            this.lbLabelCompra = new System.Windows.Forms.Label();
            this.txtPrecioCom = new System.Windows.Forms.TextBox();
            this.txtPrecioVen = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.linkConfiguraUnidadesEquivalentes = new System.Windows.Forms.LinkLabel();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnGuardar = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.txtMaxPorcDesc = new System.Windows.Forms.TextBox();
            this.txtPrecioCata = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtComision = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbDetraccion = new System.Windows.Forms.CheckBox();
            this.btnUnidad = new System.Windows.Forms.Button();
            this.btnMarca = new System.Windows.Forms.Button();
            this.btnGrupo = new System.Windows.Forms.Button();
            this.btnLinea = new System.Windows.Forms.Button();
            this.btnFamilia = new System.Windows.Forms.Button();
            this.btnTipoArticulo = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.cbControlStock = new System.Windows.Forms.ComboBox();
            this.cmbUnidadBase = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbTipoArticulo = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.cbMarca = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbGrupo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbLinea = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbFamilia = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbEstado = new System.Windows.Forms.CheckBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtReferencia = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodProducto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.ckbVentaTicket);
            this.groupBox1.Controls.Add(this.txtStockMinimo);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtUbicacion);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtCodigoUniversal);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txt_procentaje_retencion);
            this.groupBox1.Controls.Add(this.lb_procentaje_retencion);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.rdtExonerado);
            this.groupBox1.Controls.Add(this.rdtInafecto);
            this.groupBox1.Controls.Add(this.rdtGravado);
            this.groupBox1.Controls.Add(this.lbPrecioVenta);
            this.groupBox1.Controls.Add(this.lbLabelCompra);
            this.groupBox1.Controls.Add(this.txtPrecioCom);
            this.groupBox1.Controls.Add(this.txtPrecioVen);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.linkConfiguraUnidadesEquivalentes);
            this.groupBox1.Controls.Add(this.txtPeso);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.btnCancelar);
            this.groupBox1.Controls.Add(this.btnGuardar);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.txtMaxPorcDesc);
            this.groupBox1.Controls.Add(this.txtPrecioCata);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtComision);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbDetraccion);
            this.groupBox1.Controls.Add(this.btnUnidad);
            this.groupBox1.Controls.Add(this.btnMarca);
            this.groupBox1.Controls.Add(this.btnGrupo);
            this.groupBox1.Controls.Add(this.btnLinea);
            this.groupBox1.Controls.Add(this.btnFamilia);
            this.groupBox1.Controls.Add(this.btnTipoArticulo);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.cbControlStock);
            this.groupBox1.Controls.Add(this.cmbUnidadBase);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cbTipoArticulo);
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.cbMarca);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cbGrupo);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbLinea);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cbFamilia);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbEstado);
            this.groupBox1.Controls.Add(this.txtNombre);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtReferencia);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtCodProducto);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(587, 419);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nuevo Producto";
            // 
            // ckbVentaTicket
            // 
            this.ckbVentaTicket.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ckbVentaTicket.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ckbVentaTicket.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbVentaTicket.Location = new System.Drawing.Point(40, 360);
            this.ckbVentaTicket.Name = "ckbVentaTicket";
            this.ckbVentaTicket.Size = new System.Drawing.Size(138, 23);
            this.ckbVentaTicket.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ckbVentaTicket.TabIndex = 135;
            this.ckbVentaTicket.Text = "VENTA CON TICKET";
            // 
            // txtStockMinimo
            // 
            this.txtStockMinimo.Location = new System.Drawing.Point(129, 318);
            this.txtStockMinimo.Name = "txtStockMinimo";
            this.txtStockMinimo.Size = new System.Drawing.Size(121, 20);
            this.txtStockMinimo.TabIndex = 131;
            this.txtStockMinimo.Text = "0.00";
            this.txtStockMinimo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 321);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 13);
            this.label17.TabIndex = 130;
            this.label17.Text = "Stock Mínimo:";
            // 
            // txtUbicacion
            // 
            this.txtUbicacion.Location = new System.Drawing.Point(376, 41);
            this.txtUbicacion.Name = "txtUbicacion";
            this.txtUbicacion.Size = new System.Drawing.Size(195, 20);
            this.txtUbicacion.TabIndex = 129;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(373, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(116, 13);
            this.label16.TabIndex = 128;
            this.label16.Text = "Ubicación en almacén:";
            // 
            // txtCodigoUniversal
            // 
            this.txtCodigoUniversal.Location = new System.Drawing.Point(196, 41);
            this.txtCodigoUniversal.Name = "txtCodigoUniversal";
            this.txtCodigoUniversal.Size = new System.Drawing.Size(174, 20);
            this.txtCodigoUniversal.TabIndex = 127;
            this.txtCodigoUniversal.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(193, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 126;
            this.label11.Text = "Código Sunat:";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // txt_procentaje_retencion
            // 
            this.txt_procentaje_retencion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_procentaje_retencion.Location = new System.Drawing.Point(427, 325);
            this.txt_procentaje_retencion.MaxLength = 1000;
            this.txt_procentaje_retencion.Name = "txt_procentaje_retencion";
            this.txt_procentaje_retencion.Size = new System.Drawing.Size(144, 20);
            this.txt_procentaje_retencion.TabIndex = 125;
            this.txt_procentaje_retencion.Text = "0.00";
            this.txt_procentaje_retencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_procentaje_retencion.Visible = false;
            this.txt_procentaje_retencion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_procentaje_retencion_KeyPress);
            this.txt_procentaje_retencion.Leave += new System.EventHandler(this.txt_procentaje_retencion_Leave);
            // 
            // lb_procentaje_retencion
            // 
            this.lb_procentaje_retencion.AutoSize = true;
            this.lb_procentaje_retencion.Location = new System.Drawing.Point(282, 328);
            this.lb_procentaje_retencion.Name = "lb_procentaje_retencion";
            this.lb_procentaje_retencion.Size = new System.Drawing.Size(131, 13);
            this.lb_procentaje_retencion.TabIndex = 124;
            this.lb_procentaje_retencion.Text = "Porcentaje de Detracción:";
            this.lb_procentaje_retencion.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(282, 124);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(131, 13);
            this.label15.TabIndex = 123;
            this.label15.Text = "Producto se define como :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(556, 157);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 13);
            this.label8.TabIndex = 51;
            this.label8.Text = "%";
            this.label8.Visible = false;
            // 
            // rdtExonerado
            // 
            this.rdtExonerado.AutoSize = true;
            this.rdtExonerado.Location = new System.Drawing.Point(291, 163);
            this.rdtExonerado.Name = "rdtExonerado";
            this.rdtExonerado.Size = new System.Drawing.Size(93, 17);
            this.rdtExonerado.TabIndex = 122;
            this.rdtExonerado.Text = "EXONERADO";
            this.rdtExonerado.UseVisualStyleBackColor = true;
            this.rdtExonerado.CheckedChanged += new System.EventHandler(this.rdtExonerado_CheckedChanged);
            // 
            // rdtInafecto
            // 
            this.rdtInafecto.AutoSize = true;
            this.rdtInafecto.Location = new System.Drawing.Point(291, 185);
            this.rdtInafecto.Name = "rdtInafecto";
            this.rdtInafecto.Size = new System.Drawing.Size(78, 17);
            this.rdtInafecto.TabIndex = 121;
            this.rdtInafecto.Text = "INAFECTO";
            this.rdtInafecto.UseVisualStyleBackColor = true;
            this.rdtInafecto.CheckedChanged += new System.EventHandler(this.rdtInafecto_CheckedChanged);
            // 
            // rdtGravado
            // 
            this.rdtGravado.AutoSize = true;
            this.rdtGravado.Checked = true;
            this.rdtGravado.Location = new System.Drawing.Point(292, 141);
            this.rdtGravado.Name = "rdtGravado";
            this.rdtGravado.Size = new System.Drawing.Size(78, 17);
            this.rdtGravado.TabIndex = 120;
            this.rdtGravado.TabStop = true;
            this.rdtGravado.Text = "GRAVADO";
            this.rdtGravado.UseVisualStyleBackColor = true;
            this.rdtGravado.CheckedChanged += new System.EventHandler(this.rdtGravado_CheckedChanged);
            // 
            // lbPrecioVenta
            // 
            this.lbPrecioVenta.AutoSize = true;
            this.lbPrecioVenta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.lbPrecioVenta.ForeColor = System.Drawing.Color.Black;
            this.lbPrecioVenta.Location = new System.Drawing.Point(15, 295);
            this.lbPrecioVenta.Name = "lbPrecioVenta";
            this.lbPrecioVenta.Size = new System.Drawing.Size(113, 13);
            this.lbPrecioVenta.TabIndex = 112;
            this.lbPrecioVenta.Text = "Precio Venta con IGV:";
            // 
            // lbLabelCompra
            // 
            this.lbLabelCompra.AutoSize = true;
            this.lbLabelCompra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.lbLabelCompra.ForeColor = System.Drawing.Color.Black;
            this.lbLabelCompra.Location = new System.Drawing.Point(15, 269);
            this.lbLabelCompra.Name = "lbLabelCompra";
            this.lbLabelCompra.Size = new System.Drawing.Size(121, 13);
            this.lbLabelCompra.TabIndex = 114;
            this.lbLabelCompra.Text = "Precio Compra con IGV:";
            // 
            // txtPrecioCom
            // 
            this.txtPrecioCom.BackColor = System.Drawing.Color.White;
            this.txtPrecioCom.ForeColor = System.Drawing.Color.Black;
            this.txtPrecioCom.Location = new System.Drawing.Point(139, 266);
            this.txtPrecioCom.Name = "txtPrecioCom";
            this.txtPrecioCom.Size = new System.Drawing.Size(111, 20);
            this.txtPrecioCom.TabIndex = 115;
            this.txtPrecioCom.Text = "0.00";
            this.txtPrecioCom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPrecioVen
            // 
            this.txtPrecioVen.BackColor = System.Drawing.Color.White;
            this.txtPrecioVen.ForeColor = System.Drawing.Color.Black;
            this.txtPrecioVen.Location = new System.Drawing.Point(139, 292);
            this.txtPrecioVen.Name = "txtPrecioVen";
            this.txtPrecioVen.Size = new System.Drawing.Size(111, 20);
            this.txtPrecioVen.TabIndex = 113;
            this.txtPrecioVen.Text = "0.00";
            this.txtPrecioVen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPrecioVen.TextChanged += new System.EventHandler(this.txtPrecioVen_TextChanged);
            this.txtPrecioVen.Leave += new System.EventHandler(this.txtPrecioVen_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(535, 273);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 13);
            this.label14.TabIndex = 59;
            this.label14.Text = "%";
            this.label14.Visible = false;
            // 
            // linkConfiguraUnidadesEquivalentes
            // 
            this.linkConfiguraUnidadesEquivalentes.AutoSize = true;
            this.linkConfiguraUnidadesEquivalentes.Location = new System.Drawing.Point(37, 389);
            this.linkConfiguraUnidadesEquivalentes.Name = "linkConfiguraUnidadesEquivalentes";
            this.linkConfiguraUnidadesEquivalentes.Size = new System.Drawing.Size(213, 13);
            this.linkConfiguraUnidadesEquivalentes.TabIndex = 58;
            this.linkConfiguraUnidadesEquivalentes.TabStop = true;
            this.linkConfiguraUnidadesEquivalentes.Text = "Configurar Unidades Equivalentes y Precios";
            this.linkConfiguraUnidadesEquivalentes.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkConfiguraUnidadesEquivalentes_LinkClicked);
            // 
            // txtPeso
            // 
            this.txtPeso.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPeso.Location = new System.Drawing.Point(487, 121);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(84, 20);
            this.txtPeso.TabIndex = 56;
            this.txtPeso.Text = "0.00";
            this.txtPeso.Visible = false;
            this.txtPeso.TextChanged += new System.EventHandler(this.txtPeso_TextChanged);
            this.txtPeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPeso_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(424, 124);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 57;
            this.label12.Text = "Peso (Kg) :";
            this.label12.Visible = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageIndex = 5;
            this.btnCancelar.ImageList = this.imageList1;
            this.btnCancelar.Location = new System.Drawing.Point(509, 373);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(64, 32);
            this.btnCancelar.TabIndex = 2;
            this.btnCancelar.Text = "Salir";
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Write Document.png");
            this.imageList1.Images.SetKeyName(1, "New Document.png");
            this.imageList1.Images.SetKeyName(2, "Remove Document.png");
            this.imageList1.Images.SetKeyName(3, "document-print.png");
            this.imageList1.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
            this.imageList1.Images.SetKeyName(5, "exit.png");
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.ImageIndex = 4;
            this.btnGuardar.ImageList = this.imageList1;
            this.btnGuardar.Location = new System.Drawing.Point(425, 373);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(78, 32);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(282, 272);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(103, 13);
            this.label31.TabIndex = 16;
            this.label31.Text = "Máximo Porc. dscto:";
            this.label31.Visible = false;
            // 
            // txtMaxPorcDesc
            // 
            this.txtMaxPorcDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaxPorcDesc.Location = new System.Drawing.Point(427, 269);
            this.txtMaxPorcDesc.MaxLength = 3;
            this.txtMaxPorcDesc.Name = "txtMaxPorcDesc";
            this.txtMaxPorcDesc.Size = new System.Drawing.Size(102, 20);
            this.txtMaxPorcDesc.TabIndex = 18;
            this.txtMaxPorcDesc.Text = "0.00";
            this.txtMaxPorcDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMaxPorcDesc.Visible = false;
            this.txtMaxPorcDesc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            this.txtMaxPorcDesc.Leave += new System.EventHandler(this.txtMaxPorcDesc_Leave);
            // 
            // txtPrecioCata
            // 
            this.txtPrecioCata.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrecioCata.Location = new System.Drawing.Point(427, 295);
            this.txtPrecioCata.MaxLength = 9;
            this.txtPrecioCata.Name = "txtPrecioCata";
            this.txtPrecioCata.Size = new System.Drawing.Size(144, 20);
            this.txtPrecioCata.TabIndex = 17;
            this.txtPrecioCata.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPrecioCata.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecioCata_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(282, 299);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 52;
            this.label9.Text = "Precio Catálogo :";
            // 
            // txtComision
            // 
            this.txtComision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComision.Location = new System.Drawing.Point(487, 154);
            this.txtComision.Name = "txtComision";
            this.txtComision.Size = new System.Drawing.Size(63, 20);
            this.txtComision.TabIndex = 9;
            this.txtComision.Text = "0.00";
            this.txtComision.Visible = false;
            this.txtComision.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtComision_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(424, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 49;
            this.label7.Text = "Comisión :";
            this.label7.Visible = false;
            // 
            // cbDetraccion
            // 
            this.cbDetraccion.AutoSize = true;
            this.cbDetraccion.Location = new System.Drawing.Point(427, 184);
            this.cbDetraccion.Name = "cbDetraccion";
            this.cbDetraccion.Size = new System.Drawing.Size(121, 17);
            this.cbDetraccion.TabIndex = 13;
            this.cbDetraccion.Text = "Afecto a Detraccion";
            this.cbDetraccion.UseVisualStyleBackColor = true;
            this.cbDetraccion.Visible = false;
            // 
            // btnUnidad
            // 
            this.btnUnidad.Location = new System.Drawing.Point(398, 214);
            this.btnUnidad.Name = "btnUnidad";
            this.btnUnidad.Size = new System.Drawing.Size(23, 23);
            this.btnUnidad.TabIndex = 14;
            this.btnUnidad.Text = ">";
            this.btnUnidad.UseVisualStyleBackColor = true;
            this.btnUnidad.Click += new System.EventHandler(this.btnUnidad_Click);
            // 
            // btnMarca
            // 
            this.btnMarca.Location = new System.Drawing.Point(100, 232);
            this.btnMarca.Name = "btnMarca";
            this.btnMarca.Size = new System.Drawing.Size(23, 23);
            this.btnMarca.TabIndex = 40;
            this.btnMarca.Text = ">";
            this.btnMarca.UseVisualStyleBackColor = true;
            this.btnMarca.Click += new System.EventHandler(this.btnMarca_Click);
            // 
            // btnGrupo
            // 
            this.btnGrupo.Enabled = false;
            this.btnGrupo.Location = new System.Drawing.Point(100, 205);
            this.btnGrupo.Name = "btnGrupo";
            this.btnGrupo.Size = new System.Drawing.Size(23, 23);
            this.btnGrupo.TabIndex = 39;
            this.btnGrupo.Text = ">";
            this.btnGrupo.UseVisualStyleBackColor = true;
            this.btnGrupo.Click += new System.EventHandler(this.btnGrupo_Click);
            // 
            // btnLinea
            // 
            this.btnLinea.Enabled = false;
            this.btnLinea.Location = new System.Drawing.Point(100, 179);
            this.btnLinea.Name = "btnLinea";
            this.btnLinea.Size = new System.Drawing.Size(23, 23);
            this.btnLinea.TabIndex = 38;
            this.btnLinea.Text = ">";
            this.btnLinea.UseVisualStyleBackColor = true;
            this.btnLinea.Click += new System.EventHandler(this.btnLinea_Click);
            // 
            // btnFamilia
            // 
            this.btnFamilia.Location = new System.Drawing.Point(100, 151);
            this.btnFamilia.Name = "btnFamilia";
            this.btnFamilia.Size = new System.Drawing.Size(23, 23);
            this.btnFamilia.TabIndex = 37;
            this.btnFamilia.Text = ">";
            this.btnFamilia.UseVisualStyleBackColor = true;
            this.btnFamilia.Click += new System.EventHandler(this.btnFamilia_Click);
            // 
            // btnTipoArticulo
            // 
            this.btnTipoArticulo.Location = new System.Drawing.Point(100, 123);
            this.btnTipoArticulo.Name = "btnTipoArticulo";
            this.btnTipoArticulo.Size = new System.Drawing.Size(23, 23);
            this.btnTipoArticulo.TabIndex = 36;
            this.btnTipoArticulo.Text = ">";
            this.btnTipoArticulo.UseVisualStyleBackColor = true;
            this.btnTipoArticulo.Click += new System.EventHandler(this.btnTipoArticulo_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(282, 245);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 13);
            this.label38.TabIndex = 35;
            this.label38.Text = "Control Stock :";
            // 
            // cbControlStock
            // 
            this.cbControlStock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbControlStock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbControlStock.DisplayMember = "1,2,3,4";
            this.cbControlStock.FormattingEnabled = true;
            this.cbControlStock.Items.AddRange(new object[] {
            "LIBRE"});
            this.cbControlStock.Location = new System.Drawing.Point(427, 242);
            this.cbControlStock.Name = "cbControlStock";
            this.cbControlStock.Size = new System.Drawing.Size(144, 21);
            this.cbControlStock.TabIndex = 16;
            // 
            // cmbUnidadBase
            // 
            this.cmbUnidadBase.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbUnidadBase.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbUnidadBase.FormattingEnabled = true;
            this.cmbUnidadBase.Location = new System.Drawing.Point(427, 216);
            this.cmbUnidadBase.Name = "cmbUnidadBase";
            this.cmbUnidadBase.Size = new System.Drawing.Size(144, 21);
            this.cmbUnidadBase.TabIndex = 15;
            this.cmbUnidadBase.Tag = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(282, 219);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Unidad Base * :";
            // 
            // cbTipoArticulo
            // 
            this.cbTipoArticulo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbTipoArticulo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTipoArticulo.FormattingEnabled = true;
            this.cbTipoArticulo.Location = new System.Drawing.Point(129, 125);
            this.cbTipoArticulo.Name = "cbTipoArticulo";
            this.cbTipoArticulo.Size = new System.Drawing.Size(121, 21);
            this.cbTipoArticulo.TabIndex = 4;
            this.cbTipoArticulo.Tag = "1";
            this.cbTipoArticulo.SelectedIndexChanged += new System.EventHandler(this.cbTipoArticulo_SelectedIndexChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(15, 128);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(81, 13);
            this.label36.TabIndex = 24;
            this.label36.Text = "Tipo Artículo * :";
            // 
            // cbMarca
            // 
            this.cbMarca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbMarca.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbMarca.FormattingEnabled = true;
            this.cbMarca.Location = new System.Drawing.Point(129, 234);
            this.cbMarca.Name = "cbMarca";
            this.cbMarca.Size = new System.Drawing.Size(121, 21);
            this.cbMarca.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 237);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Marca :";
            // 
            // cbGrupo
            // 
            this.cbGrupo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbGrupo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbGrupo.Enabled = false;
            this.cbGrupo.FormattingEnabled = true;
            this.cbGrupo.Location = new System.Drawing.Point(129, 207);
            this.cbGrupo.Name = "cbGrupo";
            this.cbGrupo.Size = new System.Drawing.Size(121, 21);
            this.cbGrupo.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Modelo :";
            // 
            // cbLinea
            // 
            this.cbLinea.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbLinea.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbLinea.Enabled = false;
            this.cbLinea.FormattingEnabled = true;
            this.cbLinea.Location = new System.Drawing.Point(129, 180);
            this.cbLinea.Name = "cbLinea";
            this.cbLinea.Size = new System.Drawing.Size(121, 21);
            this.cbLinea.TabIndex = 6;
            this.cbLinea.SelectionChangeCommitted += new System.EventHandler(this.cbLinea_SelectionChangeCommitted);
            this.cbLinea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbLinea_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 183);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Categoria :";
            // 
            // cbFamilia
            // 
            this.cbFamilia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbFamilia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbFamilia.FormattingEnabled = true;
            this.cbFamilia.Location = new System.Drawing.Point(129, 153);
            this.cbFamilia.Name = "cbFamilia";
            this.cbFamilia.Size = new System.Drawing.Size(121, 21);
            this.cbFamilia.TabIndex = 5;
            this.cbFamilia.Tag = "1";
            this.cbFamilia.SelectionChangeCommitted += new System.EventHandler(this.cbFamilia_SelectionChangeCommitted);
            this.cbFamilia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbFamilia_KeyDown);
            this.cbFamilia.Leave += new System.EventHandler(this.cbFamilia_SelectionChangeCommitted);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Familia * :";
            // 
            // cbEstado
            // 
            this.cbEstado.AutoSize = true;
            this.cbEstado.Checked = true;
            this.cbEstado.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEstado.Location = new System.Drawing.Point(517, 19);
            this.cbEstado.Name = "cbEstado";
            this.cbEstado.Size = new System.Drawing.Size(56, 17);
            this.cbEstado.TabIndex = 1;
            this.cbEstado.Text = "Activo";
            this.cbEstado.UseVisualStyleBackColor = true;
            // 
            // txtNombre
            // 
            this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombre.Location = new System.Drawing.Point(18, 83);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(553, 20);
            this.txtNombre.TabIndex = 3;
            this.txtNombre.Tag = "1";
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Nombre * :";
            // 
            // txtReferencia
            // 
            this.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReferencia.Location = new System.Drawing.Point(90, 41);
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.Size = new System.Drawing.Size(100, 20);
            this.txtReferencia.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Código: ";
            // 
            // txtCodProducto
            // 
            this.txtCodProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodProducto.Enabled = false;
            this.txtCodProducto.Location = new System.Drawing.Point(18, 41);
            this.txtCodProducto.Name = "txtCodProducto";
            this.txtCodProducto.ReadOnly = true;
            this.txtCodProducto.Size = new System.Drawing.Size(66, 20);
            this.txtCodProducto.TabIndex = 0;
            this.txtCodProducto.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Item: ";
            this.label1.Visible = false;
            // 
            // frmRegistroProducto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(612, 436);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRegistroProducto";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Registro Producto";
            this.Load += new System.EventHandler(this.frmRegistroProducto_Load);
            this.Shown += new System.EventHandler(this.frmRegistroProducto_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.CheckBox cbEstado;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtReferencia;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCodProducto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbTipoArticulo;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cbMarca;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbGrupo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbLinea;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbFamilia;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbUnidadBase;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cbControlStock;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnUnidad;
        private System.Windows.Forms.Button btnMarca;
        private System.Windows.Forms.Button btnGrupo;
        private System.Windows.Forms.Button btnLinea;
        private System.Windows.Forms.Button btnFamilia;
        private System.Windows.Forms.Button btnTipoArticulo;
        private System.Windows.Forms.CheckBox cbDetraccion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtComision;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPrecioCata;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMaxPorcDesc;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.LinkLabel linkConfiguraUnidadesEquivalentes;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPrecioCom;
        private System.Windows.Forms.Label lbLabelCompra;
        private System.Windows.Forms.TextBox txtPrecioVen;
        private System.Windows.Forms.Label lbPrecioVenta;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton rdtExonerado;
        private System.Windows.Forms.RadioButton rdtInafecto;
        private System.Windows.Forms.RadioButton rdtGravado;
        private System.Windows.Forms.TextBox txt_procentaje_retencion;
        private System.Windows.Forms.Label lb_procentaje_retencion;
		private System.Windows.Forms.TextBox txtUbicacion;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox txtCodigoUniversal;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox txtStockMinimo;
		private System.Windows.Forms.Label label17;
        public DevComponents.DotNetBar.Controls.CheckBoxX ckbVentaTicket;
    }
}