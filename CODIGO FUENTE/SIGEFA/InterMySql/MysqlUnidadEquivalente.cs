﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using SIGEFA.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.InterMySql
{
    class MysqlUnidadEquivalente : IUnidadEquivalente
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        public DataTable listar_unidad_equivalente()
        {

            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("listar_unidad_equivalente", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;                
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
    
}
