﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using SIGEFA.Entidades;
using SIGEFA.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SIGEFA.InterMySql
{
	class MysqlNotaIngresoIgv : INotaIngresoIgv
	{
		clsConexionMysql con = new clsConexionMysql();
		MySqlCommand cmd = null;
		MySqlDataReader dr = null;
		MySqlDataAdapter adap = null;
		DataTable tabla = null;

		public bool insert(clsNotaIngresoIgv notaIngresoIgv)
		{
			try
			{
				con.conectarBD();

				cmd = new MySqlCommand("GuardaNotaIngresoIgv", con.conector);
				cmd.CommandType = CommandType.StoredProcedure;
				MySqlParameter oParam;
				oParam = cmd.Parameters.AddWithValue("codigo_nota_ingreso", notaIngresoIgv.CodNotaIngreso);
				oParam = cmd.Parameters.AddWithValue("igv", notaIngresoIgv.IncluyeIgv);
				oParam = cmd.Parameters.AddWithValue("newid", 0);
				oParam.Direction = ParameterDirection.Output;
				int x = cmd.ExecuteNonQuery();

				notaIngresoIgv.CodNotaIngresoIgv = Convert.ToInt32(cmd.Parameters["newid"].Value);

				if (x != 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (MySqlException ex)
			{
				throw ex;
			}
			finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
		}
	}
}
