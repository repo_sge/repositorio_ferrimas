﻿using SIGEFA.Administradores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SIGEFA.Reportes
{
	public partial class frmParamVentaTotalizadoResumido : DevComponents.DotNetBar.OfficeForm
	{
		clsAdmFacturaVenta admFacturaVenta = new clsAdmFacturaVenta();
		public static BindingSource data = new BindingSource();
		String filtro = String.Empty;

		public frmParamVentaTotalizadoResumido()
		{
			InitializeComponent();
		}

		private void frmParamVentaTotalizadoResumido_Load(object sender, EventArgs e)
		{

		}

		private void btnBuscar_Click(object sender, EventArgs e)
		{
			BuscarVentas();
			lblCantidadRegistros.Text = "Ventas Encontradas: " + dgvVentasTotalizado.RowCount;
			lblCantidadRegistros.Visible = true;
			CalcularTotalVentas();
		}

		private void BuscarVentas()
		{
			/*dgvVentasTotalizado.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
			dgvVentasTotalizado.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
			this.dgvVentasTotalizado.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 10);*/

			dgvVentasTotalizado.DataSource = data;
			data.DataSource = admFacturaVenta.ReporteVentasResumido(dtpDesde.Value.Date, dtpHasta.Value.Date);
			data.Filter = String.Empty;
			filtro = String.Empty;
			dgvVentasTotalizado.ClearSelection();
			if (dgvVentasTotalizado.Rows.Count == 0)
			{
				MessageBox.Show("No se encontraron resultados con los parámetros seleccionados",
								"Reporte de Ventas Resumido",MessageBoxButtons.OK,MessageBoxIcon.Information);
			}
		}

		private void CalcularTotalVentas()
		{
			if (dgvVentasTotalizado.Rows.Count > 0)
			{
				lblTotalVentas.Text = "TOTAL: S/." + String.Format("{0:#,##0.00}", (dgvVentasTotalizado.Rows.Cast<DataGridViewRow>().Select(
										x => decimal.Parse(x.Cells["total"].Value.ToString())).Sum()).ToString());
			}
			else
			{
				lblTotalVentas.Text = "0.00";
			}
			lblTotalVentas.Visible = true;
		}

		private void btnCopiar_Click(object sender, EventArgs e)
		{
			try
			{
				if (dgvVentasTotalizado.Rows.Count > 0)
				{
					dgvVentasTotalizado.MultiSelect = true;
					dgvVentasTotalizado.SelectAll();
					dgvVentasTotalizado.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
					DataObject dataObj = dgvVentasTotalizado.GetClipboardContent();
					if (dataObj != null)
						Clipboard.SetDataObject(dataObj);

					dgvVentasTotalizado.MultiSelect = false;

					MessageBox.Show("Puede copiarlo a cualquier editor de texto...", "Información");
				}

			}
			catch (Exception ex) { MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information); }
		}

		private void dtpDesde_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				btnBuscar.PerformClick();
			}
		}

		private void dtpHasta_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				btnBuscar.PerformClick();
			}
		}
	}
}
