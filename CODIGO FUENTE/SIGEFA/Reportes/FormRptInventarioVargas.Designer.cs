﻿namespace SIGEFA.Reportes
{
    partial class FormRptInventarioVargas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crvReporteInventarioVargas = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crvReporteInventarioVargas
            // 
            this.crvReporteInventarioVargas.ActiveViewIndex = -1;
            this.crvReporteInventarioVargas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvReporteInventarioVargas.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvReporteInventarioVargas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crvReporteInventarioVargas.Location = new System.Drawing.Point(0, 0);
            this.crvReporteInventarioVargas.Name = "crvReporteInventarioVargas";
            this.crvReporteInventarioVargas.Size = new System.Drawing.Size(604, 328);
            this.crvReporteInventarioVargas.TabIndex = 1;
            // 
            // FormRptInventarioVargas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 328);
            this.Controls.Add(this.crvReporteInventarioVargas);
            this.DoubleBuffered = true;
            this.Name = "FormRptInventarioVargas";
            this.Text = "Reporte Inventario";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormRptInventarioVargas_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public CrystalDecisions.Windows.Forms.CrystalReportViewer crvReporteInventarioVargas;
    }
}