﻿namespace SIGEFA.Reportes
{
    partial class frmOrdendeDespachoReporte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOrdendeDespachoReporte));
            this.crvOrdenDespacho = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crvOrdenDespacho
            // 
            this.crvOrdenDespacho.ActiveViewIndex = -1;
            this.crvOrdenDespacho.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvOrdenDespacho.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvOrdenDespacho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crvOrdenDespacho.Location = new System.Drawing.Point(0, 0);
            this.crvOrdenDespacho.Name = "crvOrdenDespacho";
            this.crvOrdenDespacho.Size = new System.Drawing.Size(461, 261);
            this.crvOrdenDespacho.TabIndex = 0;
            this.crvOrdenDespacho.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // frmOrdendeDespachoReporte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 261);
            this.Controls.Add(this.crvOrdenDespacho);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmOrdendeDespachoReporte";
            this.Text = "Reporte Orden de Despacho";
            this.Load += new System.EventHandler(this.frmOrdendeDespachoReporte_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public CrystalDecisions.Windows.Forms.CrystalReportViewer crvOrdenDespacho;

    }
}