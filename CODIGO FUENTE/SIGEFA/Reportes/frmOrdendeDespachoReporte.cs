﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SIGEFA.Reportes
{
    public partial class frmOrdendeDespachoReporte : Form
    {
        public DataTable DTable;
        DataSet set;
        public frmOrdendeDespachoReporte()
        {
            InitializeComponent();
        }

        private void frmOrdendeDespachoReporte_Load(object sender, EventArgs e)
        {
            set = new DataSet();
            set.Tables.Add(DTable);
            set.WriteXml("C:\\XML\\ReporteOrdendeDespacho.xml", XmlWriteMode.WriteSchema);

            CROrdendeDespacho CRep = new CROrdendeDespacho();
            CRep.Load("CROrdendeDespacho.rpt");
            CRep.SetDataSource(set);
            crvOrdenDespacho.ReportSource = CRep;
        }
    }
}
